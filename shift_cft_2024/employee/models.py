from datetime import date, datetime

from sqlalchemy import ForeignKey, UniqueConstraint, func
from sqlalchemy.orm import Mapped, mapped_column, relationship

from shift_cft_2024.core.db_manager import Base


class Employee(Base):
    __tablename__ = "employees"

    id: Mapped[int] = mapped_column(primary_key=True)
    user_id: Mapped[int] = mapped_column(ForeignKey("users.id"), unique=True)
    salary_amount: Mapped[int] = mapped_column()
    next_promotion_date: Mapped[date] = mapped_column()
    update_date_time: Mapped[datetime] = mapped_column(
        server_default=func.now(), server_onupdate=func.now()
    )

    user: Mapped["User"] = relationship(cascade="save-update, merge", back_populates="employee")

    @property
    def user_first_name(self):
        return self.user.first_name

    @property
    def user_last_name(self):
        return self.user.last_name

    __table_args__ = (UniqueConstraint(user_id),)
