from sqlalchemy.ext.asyncio import AsyncSession

from shift_cft_2024.core.exceptions import EntityNotFoundException, ForbiddenException
from shift_cft_2024.core.schemas import PageSchema
from shift_cft_2024.employee.models import Employee
from shift_cft_2024.employee.repositories import EmployeeRepository
from shift_cft_2024.employee.schemas import EmployeeUpdateSchema
from shift_cft_2024.users.models import Role
from shift_cft_2024.users.repositories import UserRepository


class EmployeeService:
    def __init__(
        self, user_repository: UserRepository, employee_repository: EmployeeRepository
    ):
        self._user_repository = user_repository
        self._employee_repository = employee_repository

    async def get_by_user_id(self, session: AsyncSession, user_id: int):
        employee = await self._employee_repository.get_by_user_id(session, user_id)
        if employee is None:
            raise EntityNotFoundException()

        return employee

    async def get_all(self, session: AsyncSession, page: int, size: int):
        employees = await self._employee_repository.get_all(session, page, size)
        total_employees_count = await self._employee_repository.count_all(session)

        return PageSchema(
            total_items_count=total_employees_count, items=list(employees)
        )

    async def update_by_user_id(
        self, session: AsyncSession, user_id: int, schema: EmployeeUpdateSchema
    ) -> Employee:
        user = await self._user_repository.get_by_id(session, user_id)
        if user is None:
            raise EntityNotFoundException()

        if user.role != Role.EMPLOYEE:
            raise ForbiddenException("User is not employee")

        employee: Employee | None = await user.awaitable_attrs.employee

        if employee is not None:
            employee.salary_amount = schema.salary_amount
            employee.next_promotion_date = schema.next_promotion_date
        else:
            employee = Employee(**schema.model_dump())

        user.employee = employee
        user = await self._user_repository.save(session, user)

        return await self._employee_repository.get_by_user_id(session, user_id)
