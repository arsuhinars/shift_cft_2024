from datetime import date, datetime
from typing import Annotated

from pydantic import BaseModel, ConfigDict, Field


class EmployeeSchema(BaseModel):
    model_config = ConfigDict(from_attributes=True)

    user_id: int
    salary_amount: int
    next_promotion_date: date
    update_date_time: datetime


class EmployeeFullSchema(BaseModel):
    model_config = ConfigDict(from_attributes=True)

    user_id: int
    user_first_name: str
    user_last_name: str
    salary_amount: int
    next_promotion_date: date
    update_date_time: datetime


class EmployeeUpdateSchema(BaseModel):
    salary_amount: Annotated[int, Field(gt=0)]
    next_promotion_date: date
