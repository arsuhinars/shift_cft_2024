from sqlalchemy import func, select
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import joinedload

from shift_cft_2024.employee.models import Employee
from shift_cft_2024.users.models import Role, User


class EmployeeRepository:
    async def get_by_user_id(self, session: AsyncSession, user_id: int):
        q = (
            select(Employee)
            .join(User, Employee.user_id == User.id)
            .where((Employee.user_id == user_id) & (User.role == Role.EMPLOYEE))
            .options(joinedload(Employee.user))
        )
        s = await session.execute(q)
        return s.scalar_one_or_none()

    async def get_all(self, session: AsyncSession, page: int, size: int):
        q = (
            select(Employee)
            .join(User, Employee.user_id == User.id)
            .where(User.role == Role.EMPLOYEE)
            .offset(page * size)
            .limit(size)
            .options(joinedload(Employee.user))
        )
        s = await session.execute(q)
        return s.scalars().all()

    async def count_all(self, session: AsyncSession):
        q = select(func.count()).select_from(Employee)
        s = await session.execute(q)
        return s.scalar_one()
