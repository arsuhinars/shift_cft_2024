from fastapi import APIRouter, Depends, status

from shift_cft_2024.auth.dependencies import AuthenticateUser, HasPermission
from shift_cft_2024.auth.permissions import IsAdmin, IsEmployee
from shift_cft_2024.core.dependencies import DbSession, EmployeeServiceDep
from shift_cft_2024.core.schemas import PageSchema
from shift_cft_2024.core.utils import IdField, PageField, SizeField
from shift_cft_2024.employee.schemas import (
    EmployeeFullSchema,
    EmployeeSchema,
    EmployeeUpdateSchema,
)

employee_router = APIRouter(prefix="/employees", tags=["Сотрудники"])


@employee_router.get(
    "/current",
    summary="Получить данные текущего авторизованного сотрудника",
    response_model=EmployeeSchema,
    dependencies=[Depends(HasPermission(IsEmployee()))],
)
async def get_current_employee(
    session: DbSession, employee_service: EmployeeServiceDep, user: AuthenticateUser
):
    employee = await employee_service.get_by_user_id(session, user.id)
    return EmployeeSchema.model_validate(employee)


@employee_router.get(
    "/{user_id}",
    summary="Получить данные сотрудника по user_id пользователя",
    responses={
        status.HTTP_403_FORBIDDEN: {
            "description": "Пользователь не является администратором"
        },
        status.HTTP_404_NOT_FOUND: {
            "description": "Сотрудник с данным user_id не существует"
        },
    },
    dependencies=[Depends(HasPermission(IsAdmin()))],
)
async def get_employee_by_id(
    session: DbSession, employee_service: EmployeeServiceDep, user_id: IdField
):
    employee = await employee_service.get_by_user_id(session, user_id)
    return EmployeeSchema.model_validate(employee)


@employee_router.get(
    "",
    summary="Получить список всех сотрудников. Поддерживает пагинацию.",
    response_model=PageSchema[EmployeeFullSchema],
    responses={
        status.HTTP_403_FORBIDDEN: {
            "description": "Пользователь не является администратором"
        },
    },
    dependencies=[Depends(HasPermission(IsAdmin()))],
)
async def get_all_employees(
    session: DbSession,
    employee_service: EmployeeServiceDep,
    page: PageField = 0,
    size: SizeField = 10,
):
    employees = await employee_service.get_all(session, page, size)
    return PageSchema(
        total_items_count=employees.total_items_count,
        items=list(map(EmployeeFullSchema.model_validate, employees.items)),
    )


@employee_router.put(
    "/{user_id}",
    summary="Обновить данные сотрудника по user_id пользователя",
    response_model=EmployeeSchema,
    responses={
        status.HTTP_403_FORBIDDEN: {
            "description": (
                "- Пользователь не является администратором\n"
                "- Роль пользователя по `user_id` не равна `EMPLOYEE`"
            )
        },
        status.HTTP_404_NOT_FOUND: {
            "description": "Сотрудник с данным user_id не существует"
        },
    },
    dependencies=[Depends(HasPermission(IsAdmin()))],
)
async def update_employee_by_id(
    session: DbSession,
    employee_service: EmployeeServiceDep,
    user_id: IdField,
    schema: EmployeeUpdateSchema,
):
    employee = await employee_service.update_by_user_id(session, user_id, schema)
    return EmployeeSchema.model_validate(employee)
