from abc import ABC, abstractmethod

from shift_cft_2024.users.models import Role, User


class BasePermission(ABC):
    @abstractmethod
    def has_permission(self, user: User | None) -> bool: ...


class Anonymous(BasePermission):
    def has_permission(self, user: User | None) -> bool:
        return user is None


class Authenticated(BasePermission):
    def has_permission(self, user: User | None) -> bool:
        return user is not None


class IsAdmin(BasePermission):
    def has_permission(self, user: User | None) -> bool:
        return user is not None and user.role in [Role.ADMIN, Role.MAIN_ADMIN]


class IsEmployee(BasePermission):
    def has_permission(self, user: User | None) -> bool:
        return user is not None and user.role == Role.EMPLOYEE
