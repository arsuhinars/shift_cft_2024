from pydantic import BaseModel

from shift_cft_2024.core.utils import NonEmptyStr


class LoginCredentials(BaseModel):
    login: NonEmptyStr
    password: NonEmptyStr


class AuthTokenSchema(BaseModel):
    token: str


class AuthTokenPayload(BaseModel):
    user_id: int
