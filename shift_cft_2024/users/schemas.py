from pydantic import BaseModel, ConfigDict

from shift_cft_2024.core.utils import NonEmptyStr
from shift_cft_2024.users.models import Role


class UserSchema(BaseModel):
    model_config = ConfigDict(from_attributes=True)

    id: int
    login: str
    first_name: str
    last_name: str
    role: Role


class UserCreateSchema(BaseModel):
    login: NonEmptyStr
    password: NonEmptyStr
    first_name: NonEmptyStr
    last_name: NonEmptyStr
    role: Role


class CurrentUserUpdateSchema(BaseModel):
    login: NonEmptyStr
    first_name: NonEmptyStr
    last_name: NonEmptyStr


class UserUpdateSchema(BaseModel):
    login: NonEmptyStr
    first_name: NonEmptyStr
    last_name: NonEmptyStr
    role: Role


class UserPasswordUpdateSchema(BaseModel):
    password: NonEmptyStr
