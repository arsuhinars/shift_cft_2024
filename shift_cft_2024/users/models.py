from enum import StrEnum
from typing import Optional

from sqlalchemy import Index, UniqueConstraint
from sqlalchemy.orm import Mapped, mapped_column, relationship

from shift_cft_2024.core.db_manager import Base


class Role(StrEnum):
    EMPLOYEE = "EMPLOYEE"
    ADMIN = "ADMIN"
    MAIN_ADMIN = "MAIN_ADMIN"


class User(Base):
    __tablename__ = "users"

    id: Mapped[int] = mapped_column(primary_key=True)
    login: Mapped[str] = mapped_column(unique=True)
    first_name: Mapped[str] = mapped_column()
    last_name: Mapped[str] = mapped_column()
    role: Mapped[Role]
    password_hash: Mapped[str] = mapped_column()

    employee: Mapped[Optional["Employee"]] = relationship(
        cascade="save-update, merge, delete, delete-orphan", back_populates="user"
    )

    __table_args__ = (UniqueConstraint(login), Index("users_role_idx", "role"))
