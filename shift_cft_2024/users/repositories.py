from sqlalchemy import exists, func, select
from sqlalchemy.ext.asyncio import AsyncSession

from shift_cft_2024.users.models import User


class UserRepository:
    async def get_by_id(self, session: AsyncSession, id: int):
        return await session.get(User, id)

    async def get_by_login(self, session: AsyncSession, login: str):
        q = select(User).where(User.login == login)
        s = await session.execute(q)
        return s.scalar_one_or_none()

    async def exists_by_login(self, session: AsyncSession, login: str):
        q = select(exists().where(User.login == login))
        s = await session.execute(q)
        return s.scalar_one()

    async def get_all(self, session: AsyncSession, page: int, size: int):
        q = select(User).offset(page * size).limit(size)
        s = await session.execute(q)
        return s.scalars().all()

    async def count_all(self, session: AsyncSession):
        q = select(func.count()).select_from(User)
        s = await session.execute(q)
        return s.scalar_one()

    async def save(self, session: AsyncSession, user: User):
        session.add(user)
        await session.flush()
        await session.commit()
        return user

    async def delete(self, session: AsyncSession, user: User):
        await session.delete(user)
        await session.commit()
