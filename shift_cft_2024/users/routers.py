from fastapi import APIRouter, Depends, status

from shift_cft_2024.auth.dependencies import AuthenticateUser, HasPermission
from shift_cft_2024.auth.permissions import Authenticated, IsAdmin
from shift_cft_2024.core.dependencies import DbSession, UserServiceDep
from shift_cft_2024.core.exceptions import ForbiddenException
from shift_cft_2024.core.schemas import PageSchema
from shift_cft_2024.core.utils import IdField, PageField, SizeField
from shift_cft_2024.users.models import Role
from shift_cft_2024.users.schemas import (
    CurrentUserUpdateSchema,
    UserCreateSchema,
    UserPasswordUpdateSchema,
    UserSchema,
    UserUpdateSchema,
)

users_router = APIRouter(prefix="/users", tags=["Пользователи"])


@users_router.post(
    "",
    summary="Создать нового пользователя",
    response_model=UserSchema,
    response_description="Пользователь был успешно создан",
    status_code=status.HTTP_201_CREATED,
    responses={
        status.HTTP_403_FORBIDDEN: {
            "description": (
                "- Пользователь не является администратором\n"
                "- Если `role` равен `ADMIN` или `MAIN_ADMIN` и пользователь не "
                "является главным администратором\n"
                "- Если `role` равен `MAIN_ADMIN`"
            )
        },
        status.HTTP_409_CONFLICT: {
            "description": "Другой пользователь с указанным `login` уже существует"
        },
    },
    dependencies=[Depends(HasPermission(IsAdmin()))],
)
async def create_user(
    session: DbSession,
    user_service: UserServiceDep,
    user: AuthenticateUser,
    schema: UserCreateSchema,
):
    if schema.role == Role.MAIN_ADMIN:
        raise ForbiddenException()

    if schema.role == Role.ADMIN and user.role != Role.MAIN_ADMIN:
        raise ForbiddenException()

    user = await user_service.create(session, schema)

    return UserSchema.model_validate(user)


@users_router.get(
    "/current",
    summary="Получить текущего авторизованного пользователя",
    response_model=UserSchema,
    dependencies=[Depends(HasPermission(Authenticated()))],
)
async def get_current_user(user: AuthenticateUser):
    return UserSchema.model_validate(user)


@users_router.get(
    "/{id}",
    summary="Получить пользователя по id",
    response_model=UserSchema,
    responses={
        status.HTTP_403_FORBIDDEN: {
            "description": "Пользователь не является администратором"
        },
        status.HTTP_404_NOT_FOUND: {
            "description": "Пользователь с данным id не существует"
        },
    },
    dependencies=[Depends(HasPermission(IsAdmin()))],
)
async def get_user_by_id(session: DbSession, user_service: UserServiceDep, id: IdField):
    user = await user_service.get_by_id(session, id)
    return UserSchema.model_validate(user)


@users_router.get(
    "",
    summary="Получить список всех пользователей. Поддерживает пагинацию.",
    response_model=PageSchema[UserSchema],
    responses={
        status.HTTP_403_FORBIDDEN: {
            "description": "Пользователь не является администратором"
        }
    },
    dependencies=[Depends(HasPermission(IsAdmin()))],
)
async def get_all_users(
    session: DbSession,
    user_service: UserServiceDep,
    page: PageField = 0,
    size: SizeField = 10,
):
    users = await user_service.get_all(session, page, size)
    return PageSchema(
        total_items_count=users.total_items_count,
        items=list(map(UserSchema.model_validate, users.items)),
    )


@users_router.put(
    "/current",
    summary="Обновить текущего авторизованного пользователя",
    response_model=UserSchema,
    responses={
        status.HTTP_409_CONFLICT: {
            "description": "Другой пользователь с указанным `login` уже существует"
        }
    },
    dependencies=[Depends(HasPermission(Authenticated()))],
)
async def update_current_user(
    session: DbSession,
    user_service: UserServiceDep,
    user: AuthenticateUser,
    schema: CurrentUserUpdateSchema,
):
    update_schema = UserUpdateSchema(**schema.model_dump(), role=user.role)
    new_user = await user_service.update_by_id(session, user.id, update_schema)
    return UserSchema.model_validate(new_user)


@users_router.put(
    "/current/password",
    summary="Обновить пароль текущего авторизованного пользователя",
    response_model=UserSchema,
    dependencies=[Depends(HasPermission(Authenticated()))],
)
async def update_current_user_password(
    session: DbSession,
    user_service: UserServiceDep,
    user: AuthenticateUser,
    schema: UserPasswordUpdateSchema,
):
    user = await user_service.update_password_by_id(session, user.id, schema.password)
    return UserSchema.model_validate(user)


@users_router.put(
    "/{id}",
    summary="Обновить пользователя по id",
    response_model=UserSchema,
    responses={
        status.HTTP_403_FORBIDDEN: {
            "description": (
                "- Пользователь не является администратором\n"
                "- Попытка обновить главного администратора"
                "- Попытка обновить администратора, если пользователь не "
                "является главным администратором\n"
                "- Попытка изменить роль на `ADMIN`, если пользователь не "
                "является главным администратором\n"
                "- Попытка изменить роль на `MAIN_ADMIN`\n"
            )
        },
        status.HTTP_404_NOT_FOUND: {
            "description": "Пользователь с данным id не существует"
        },
        status.HTTP_409_CONFLICT: {
            "description": "Другой пользователь с указанным `login` уже существует"
        },
    },
    dependencies=[Depends(HasPermission(IsAdmin()))],
)
async def update_user_by_id(
    session: DbSession,
    user_service: UserServiceDep,
    current_user: AuthenticateUser,
    id: IdField,
    schema: UserUpdateSchema,
):
    user = await user_service.get_by_id(session, id)
    if user.role == Role.MAIN_ADMIN:
        raise ForbiddenException()
    elif user.role == Role.ADMIN and current_user.role != Role.MAIN_ADMIN:
        raise ForbiddenException()

    if user.role != schema.role:
        if schema.role == Role.MAIN_ADMIN:
            raise ForbiddenException()
        elif schema.role == Role.ADMIN and current_user.role != Role.MAIN_ADMIN:
            raise ForbiddenException()

    user = await user_service.update_by_id(session, id, schema)
    return UserSchema.model_validate(user)


@users_router.put(
    "/{id}/password",
    summary="Обновить пароль пользователя по id",
    response_model=UserSchema,
    responses={
        status.HTTP_403_FORBIDDEN: {
            "description": (
                "- Пользователь не является администратором\n"
                "- Попытка изменить пароль у администратора , если пользователь не "
                "является главным администратором\n"
                "- Попытка изменить пароль у главного администратора"
            )
        },
        status.HTTP_404_NOT_FOUND: {
            "description": "Пользователь с данным id не существует"
        },
    },
    dependencies=[Depends(HasPermission(IsAdmin()))],
)
async def update_user_password_by_id(
    session: DbSession,
    user_service: UserServiceDep,
    current_user: AuthenticateUser,
    id: IdField,
    schema: UserPasswordUpdateSchema,
):
    user = await user_service.get_by_id(session, id)
    if (
        user.role == Role.MAIN_ADMIN
        or user.role == Role.ADMIN
        and current_user.role != Role.MAIN_ADMIN
    ):
        raise ForbiddenException()

    user = await user_service.update_password_by_id(session, id, schema.password)
    return UserSchema.model_validate(user)


@users_router.delete(
    "/{id}",
    summary="Удалить пользователя по id",
    responses={
        status.HTTP_403_FORBIDDEN: {
            "description": (
                "- Пользователь не является администратором\n"
                "- Попытка удалить аккаунт администратора, если пользователь не "
                "является главный администратором\n"
                "- Попытка удалить аккаунт главного администратора"
            )
        },
        status.HTTP_404_NOT_FOUND: {
            "description": "Пользователь с данным id не существует"
        },
    },
    dependencies=[Depends(HasPermission(IsAdmin()))],
)
async def delete_user_by_id(
    session: DbSession,
    user_service: UserServiceDep,
    current_user: AuthenticateUser,
    id: IdField,
):
    user = await user_service.get_by_id(session, id)
    if user.role == Role.MAIN_ADMIN:
        raise ForbiddenException()

    if user.role == Role.ADMIN and current_user.role != Role.MAIN_ADMIN:
        raise ForbiddenException()

    await user_service.delete_by_id(session, id)
