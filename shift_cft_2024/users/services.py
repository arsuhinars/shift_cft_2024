from sqlalchemy.ext.asyncio import AsyncSession

from shift_cft_2024.auth.services import PasswordService
from shift_cft_2024.core.exceptions import (
    EntityAlreadyExistsException,
    EntityNotFoundException,
)
from shift_cft_2024.core.schemas import PageSchema
from shift_cft_2024.users.models import User
from shift_cft_2024.users.repositories import UserRepository
from shift_cft_2024.users.schemas import UserCreateSchema, UserUpdateSchema


class UserService:
    def __init__(
        self, password_service: PasswordService, user_repository: UserRepository
    ):
        self._password_service = password_service
        self._user_repository = user_repository

    async def create(self, session: AsyncSession, schema: UserCreateSchema):
        if await self._user_repository.exists_by_login(session, schema.login):
            raise EntityAlreadyExistsException("User with given login already exists")

        user = User(**schema.model_dump(exclude=["password"]))
        user.password_hash = self._password_service.get_password_hash(schema.password)

        return await self._user_repository.save(session, user)

    async def get_by_id(self, session: AsyncSession, id: int):
        user = await self._user_repository.get_by_id(session, id)
        if user is None:
            raise EntityNotFoundException("User with given id was not found")

        return user

    async def get_all(self, session: AsyncSession, page: int, size: int):
        users = await self._user_repository.get_all(session, page, size)
        total_users_count = await self._user_repository.count_all(session)

        return PageSchema(total_items_count=total_users_count, items=list(users))

    async def update_by_id(
        self, session: AsyncSession, id: int, schema: UserUpdateSchema
    ):
        user = await self._user_repository.get_by_id(session, id)
        if user is None:
            raise EntityNotFoundException("User with given id was not found")

        if schema.login != user.login and await self._user_repository.exists_by_login(
            session, schema.login
        ):
            raise EntityAlreadyExistsException("User with given login already exists")

        user.login = schema.login
        user.first_name = schema.first_name
        user.last_name = schema.last_name
        user.role = schema.role

        return await self._user_repository.save(session, user)

    async def update_password_by_id(
        self, session: AsyncSession, id: int, password: str
    ):
        user = await self._user_repository.get_by_id(session, id)
        if user is None:
            raise EntityNotFoundException("User with given id was not found")

        user.password_hash = self._password_service.get_password_hash(password)

        return await self._user_repository.save(session, user)

    async def delete_by_id(self, session: AsyncSession, id: int):
        user = await self._user_repository.get_by_id(session, id)
        if user is None:
            raise EntityNotFoundException("User with given id was not found")

        await self._user_repository.delete(session, user)
