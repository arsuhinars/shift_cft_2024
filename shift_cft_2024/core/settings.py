from pydantic_settings import BaseSettings, SettingsConfigDict

from shift_cft_2024.users.schemas import UserCreateSchema


class AppSettings(BaseSettings):
    model_config = SettingsConfigDict(secrets_dir="/run/secrets")

    db_url: str
    cors_allowed_origins: list[str]
    auth_token_lifetime: int = 3600
    auth_token_secret_key: str
    initial_user: UserCreateSchema | None = None
