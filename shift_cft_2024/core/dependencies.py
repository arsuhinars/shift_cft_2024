from typing import Annotated

from fastapi import Depends, Request
from sqlalchemy.ext.asyncio import AsyncSession

from shift_cft_2024.auth.services import AuthService
from shift_cft_2024.core.db_manager import DatabaseManager
from shift_cft_2024.employee.services import EmployeeService
from shift_cft_2024.users.services import UserService


def db_manager(request: Request) -> DatabaseManager:
    return request.app.state.database_manager


async def db_session(db_manager: Annotated[DatabaseManager, Depends(db_manager)]):
    async with db_manager.create_session() as session:
        yield session


def auth_service(request: Request) -> AuthService:
    return request.app.state.auth_service


def user_service(request: Request) -> UserService:
    return request.app.state.user_service


def employee_service(request: Request) -> EmployeeService:
    return request.app.state.employee_service


DbSession = Annotated[AsyncSession, Depends(db_session)]
AuthServiceDep = Annotated[AuthService, Depends(auth_service)]
UserServiceDep = Annotated[UserService, Depends(user_service)]
EmployeeServiceDep = Annotated[EmployeeService, Depends(employee_service)]
