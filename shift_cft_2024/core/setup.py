import logging
import sys
from contextlib import asynccontextmanager

from fastapi import FastAPI
from fastapi.exceptions import RequestValidationError
from fastapi.middleware.cors import CORSMiddleware

from shift_cft_2024.core.db_manager import DatabaseManager  # isort: split

from shift_cft_2024.auth.routers import auth_router
from shift_cft_2024.auth.services import AuthService, PasswordService, TokenService
from shift_cft_2024.core.exceptions import (
    AppException,
    EntityAlreadyExistsException,
    handle_app_exception,
    handle_validation_exception,
)
from shift_cft_2024.core.settings import AppSettings
from shift_cft_2024.employee.repositories import EmployeeRepository
from shift_cft_2024.employee.routers import employee_router
from shift_cft_2024.employee.services import EmployeeService
from shift_cft_2024.users.repositories import UserRepository
from shift_cft_2024.users.routers import users_router
from shift_cft_2024.users.services import UserService


def create_app(settings: AppSettings | None = None) -> FastAPI:
    if settings is None:
        settings = AppSettings()

    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)

    app = FastAPI(
        title="Сервис учета заработной платы",
        lifespan=_app_lifespan,
        servers=[
            {"url": "http://localhost:8080", "description": "Локальный сервер"},
        ],
        responses={
            400: {"description": "Неверный формат входных данных"},
        },
    )

    """ Setup global dependencies """
    _setup_app_dependencies(app, settings)

    """ Setup middlewares """
    app.add_middleware(
        CORSMiddleware,
        allow_origins=settings.cors_allowed_origins,
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

    """ Setup routers """
    app.include_router(auth_router)
    app.include_router(users_router)
    app.include_router(employee_router)

    """ Setup exception handlers """
    app.add_exception_handler(AppException, handle_app_exception)
    app.add_exception_handler(RequestValidationError, handle_validation_exception)

    return app


def _setup_app_dependencies(app: FastAPI, settings: AppSettings):
    app.state.settings = settings
    app.state.database_manager = DatabaseManager(settings.db_url)

    user_repository = UserRepository()
    employee_repository = EmployeeRepository()

    password_service = PasswordService()
    token_service = TokenService(
        settings.auth_token_secret_key, settings.auth_token_lifetime
    )
    auth_service = AuthService(password_service, token_service, user_repository)
    user_service = UserService(password_service, user_repository)
    employee_service = EmployeeService(user_repository, employee_repository)

    app.state.password_service = password_service
    app.state.auth_service = auth_service
    app.state.user_service = user_service
    app.state.employee_service = employee_service


@asynccontextmanager
async def _app_lifespan(app: FastAPI):
    settings: AppSettings = app.state.settings
    db: DatabaseManager = app.state.database_manager

    await db.initialize()

    if settings.initial_user is not None:
        user_service: UserService = app.state.user_service
        async with db.create_session() as session:
            try:
                await user_service.create(session, settings.initial_user)
                logging.info("Initial user was successfully created")
            except EntityAlreadyExistsException:
                logging.info("Initial user already exists. Skipped")

    yield
    await db.dispose()
