# Сервис учета заработной платы
REST API сервис для учета зарплаты сотрудников, а также даты следующего
повышения. В сервисе реализованы следующие функции:
- Аутентификация по уникальному краткосрочному JWT токену
- Наличие ролей: главный администратор, администратор и сотрудник
- Возможность создания, редактирования и удаления пользователей и сотрудников
    для администраторов
- Просмотр списка пользователей и сотрудников для администраторов
- Просмотр личных данных для пользователей
- Изменение личных данных для пользователей
- Просмотр данных о зарплате и дате повышении для сотрудников

## Стек
[Python](https://www.python.org/),
[Poetry](https://python-poetry.org/),
[pytest](https://docs.pytest.org/),
[FastAPI](https://fastapi.tiangolo.com/),
[SQLAlchemy](https://www.sqlalchemy.org/),
[PostgreSQL](https://www.postgresql.org/),
[Docker](https://www.docker.com/)

## Установка и запуск
1. Убедитесь, что у вас в системе установлен [Docker](https://www.docker.com/)
2. Склонируйте данный репозиторий:
```shell
git clone https://gitlab.com/arsuhinars/shift_cft_2024.git
```
3. В директории репозитория создайте `.env` файл с конфигурацией сервиса. Ниже
приведен пример данного файла:
```text
# Порт сервера
SERVER_PORT=8080
# URL для подключения сервера к БД
DB_URL=postgresql+psycopg://user:qwerty12@database:5432/shift_cft_2024
# Секретный ключ для подписи авторизационных токенов
AUTH_TOKEN_SECRET_KEY=super_secret_key
# Данные пользователя, который будет создан при первом запуске сервера
INITIAL_USER='{"login":"admin","password":"qwerty12","first_name":"Admin","last_name":"Adminovich","role":"MAIN_ADMIN"}'

# Название базы данных
POSTGRES_DATABASE=shift_cft_2024
# Имя пользователя в БД
POSTGRES_USER=user
# Пароль пользователя в БД
POSTGRES_PASSWORD=qwerty12
```
3. Перейдите в директорию репозитория и запустите сервис:
```shell
docker compose up -d
```
4. Сервис будет доступен по адресу http://localhost:8080. По адресу
http://localhost:8080/docs можно просмотреть OpenAPI документацию к сервису
5. Остановка сервиса:
```shell
docker compose down
```

## Автоматические тесты
Для запуска тестов потребуется Python версии 3.12, а также пакетный менеджер
[poetry](https://python-poetry.org/).

Установка необходимых зависимостей для тестов:
```shell
poetry install --with test
```

Команда запуска автоматических тестов:
```shell
poetry run pytest
```

В выводе будет отчет о результате работы тестов. Ниже приведен пример отчета:
```shell
============================= test session starts ==============================
platform linux -- Python 3.12.3, pytest-8.2.0, pluggy-1.5.0
rootdir: /home/.../shift_cft_2024
configfile: pytest.ini
plugins: anyio-4.3.0, asyncio-0.23.6
asyncio: mode=Mode.AUTO
collected 244 items                                                            

tests/routers/test_auth.py ........                                      [  3%]
tests/routers/test_users.py ............................................ [ 21%]
........................................................................ [ 50%]
.....                                                                    [ 52%]
tests/services/test_auth_service.py ..........................           [ 63%]
tests/services/test_employee_service.py ......................           [ 72%]
tests/services/test_password_service.py .......                          [ 75%]
tests/services/test_token_service.py ...................                 [ 83%]
tests/services/test_user_service.py .................................... [ 97%]
.....                                                                    [100%]

=============================== warnings summary ===============================
../../.cache/pypoetry/virtualenvs/shift-cft-2024-AyoduA5B-py3.12/lib/python3.12/site-packages/passlib/utils/__init__.py:854
  /home/arsuhinars/.cache/pypoetry/virtualenvs/shift-cft-2024-AyoduA5B-py3.12/lib/python3.12/site-packages/passlib/utils/__init__.py:854: DeprecationWarning: 'crypt' is deprecated and slated for removal in Python 3.13
    from crypt import crypt as _crypt

tests/routers/test_auth.py::TestLogin::test_login[ivan-vanya2004]
  /home/.../.cache/pypoetry/virtualenvs/shift-cft-2024-AyoduA5B-py3.12/lib/python3.12/site-packages/pydantic_settings/sources.py:376: UserWarning: directory "/run/secrets" does not exist
    warnings.warn(f'directory "{self.secrets_path}" does not exist')

-- Docs: https://docs.pytest.org/en/stable/how-to/capture-warnings.html
======================= 244 passed, 2 warnings in 34.62s =======================
```

## Использование сервиса
Ниже будут приведены примеры работы с сервисом с использованием консольной
программы `curl`. Вы можете воспользоваться также OpenAPI, в котором также
можно выполнять запросы прямо из браузера. Она доступна по ссылке
http://localhost:8080/docs.

1. __Авторизация__ В данном примере идет авторизация в аккаунт главного администратора.
    ```shell
    curl -X 'POST' \
        'http://localhost:8080/auth/login' \
        -H 'accept: application/json' \
        -H 'Content-Type: application/json' \
        -d '{
        "login": "admin",
        "password": "qwerty12"
    }'
    ```

    Ответ:
    ```json
    {
        "token": "TOKEN"
    }
    ```

    В `TOKEN` будет указан авторизационный токен, который необходимо будет
    использовать в следующих примерах.

2. __Получение своего аккаунта__
    ```shell
    curl -X 'GET' \
        'http://localhost:8080/users/current' \
        -H 'accept: application/json' \
        -H 'Authorization: Bearer TOKEN'
    ```

    Ответ:
    ```json
    {
        "id": 1,
        "login": "admin",
        "first_name": "Admin",
        "last_name": "Adminovich",
        "role": "MAIN_ADMIN"
    }
    ```

3. __Создание нового аккаунта__
    ```shell
    curl -X 'POST' \
        'http://localhost:8080/users' \
        -H 'accept: application/json' \
        -H 'Authorization: Bearer TOKEN' \
        -H 'Content-Type: application/json' \
        -d '{
        "login": "andrey144",
        "password": "12345678",
        "first_name": "Andrew",
        "last_name": "Ivanov",
        "role": "EMPLOYEE"
    }'
    ```

    Ответ:
    ```json
    {
        "id": 2,
        "login": "andrey144",
        "first_name": "Andrew",
        "last_name": "Ivanov",
        "role": "EMPLOYEE"
    }
    ```

4. __Обновление данных сотрудника__
    ```shell
    curl -X 'PUT' \
        'http://localhost:8080/employees/2' \
        -H 'accept: application/json' \
        -H 'Authorization: Bearer TOKEN' \
        -H 'Content-Type: application/json' \
        -d '{
        "salary_amount": 30000,
        "next_promotion_date": "2024-05-15"
    }'
    ```

    Ответ:
    ```json
    {
        "user_id": 2,
        "salary_amount": 30000,
        "next_promotion_date": "2024-05-15",
        "update_date_time": "2024-05-15T09:12:51.050037"
    }
    ```

5. __Просмотр данных от имени сотрудника__ Здесь используем токен, полученный
    после авторизации в аккаунт созданного сотрудника.
    ```shell
    curl -X 'GET' \
        'http://localhost:8080/employees/current' \
        -H 'accept: application/json' \
        -H 'Authorization: Bearer TOKEN'
    ```

    Ответ:
    ```json
    {
        "user_id": 2,
        "salary_amount": 30000,
        "next_promotion_date": "2024-05-15",
        "update_date_time": "2024-05-15T09:12:51.050037"
    }
    ```

6. __Просмотр списка сотрудников от имени администратора__
    ```shell
    curl -X 'GET' \
        'http://localhost:8080/employees?page=0&size=10' \
        -H 'accept: application/json' \
        -H 'Authorization: Bearer TOKEN'
    ```

    Ответ:
    ```json
    {
        "total_items_count": 1,
        "items": [
            {
                "user_id": 2,
                "user_first_name": "Andrew",
                "user_last_name": "Ivanov",
                "salary_amount": 30000,
                "next_promotion_date": "2024-05-15",
                "update_date_time": "2024-05-15T09:12:51.050037"
            }
        ]
    }
    ```
