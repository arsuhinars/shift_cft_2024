from shift_cft_2024.auth.schemas import LoginCredentials
from shift_cft_2024.auth.services import AuthService
from shift_cft_2024.core.db_manager import DatabaseManager


async def get_auth_token(
    db_manager: DatabaseManager, auth_service: AuthService, login: str, password: str
):
    async with db_manager.create_session() as session:
        token_schema = await auth_service.login_user(
            session, LoginCredentials(login=login, password=password)
        )
    return token_schema.token
