import pytest
from fastapi import status
from fastapi.testclient import TestClient
from sqlalchemy import func, select

from shift_cft_2024.auth.services import AuthService, PasswordService
from shift_cft_2024.core.db_manager import DatabaseManager
from shift_cft_2024.core.schemas import PageSchema
from shift_cft_2024.employee.models import Employee
from shift_cft_2024.users.models import Role, User
from shift_cft_2024.users.schemas import (
    CurrentUserUpdateSchema,
    UserCreateSchema,
    UserSchema,
    UserUpdateSchema,
)
from tests.routers.utils import get_auth_token


class TestUserCreation:
    @pytest.mark.parametrize(
        "login,password,schema",
        [
            (
                "admin",
                "admin",
                UserCreateSchema(
                    login="brada",
                    password="qwerty13",
                    first_name="Andrew",
                    last_name="Johnson",
                    role=Role.EMPLOYEE,
                ),
            ),
            (
                "admin",
                "admin",
                UserCreateSchema(
                    login="vanya337",
                    password="password12",
                    first_name="Ivan",
                    last_name="Vanov",
                    role=Role.ADMIN,
                ),
            ),
            (
                "fisherman14",
                "fishingMaster",
                UserCreateSchema(
                    login="badboy",
                    password="boy14",
                    first_name="Bogdan",
                    last_name="Bogdanovich",
                    role=Role.EMPLOYEE,
                ),
            ),
        ],
    )
    async def test_valid_creation(
        self,
        test_client: TestClient,
        db_manager: DatabaseManager,
        auth_service: AuthService,
        login: str,
        password: str,
        schema: UserCreateSchema,
    ):
        token = await get_auth_token(db_manager, auth_service, login, password)
        response = test_client.post(
            "/users",
            json=schema.model_dump(mode="json"),
            headers={"Authorization": f"Bearer {token}"},
        )

        assert response.is_success

        json: dict = response.json()
        assert json["login"] == schema.login
        assert json["first_name"] == schema.first_name
        assert json["last_name"] == schema.last_name
        assert json["role"] == schema.role

    @pytest.mark.parametrize(
        "login,password,schema",
        [
            (
                "admin",
                "admin",
                UserCreateSchema(
                    login="fisherman14",
                    password="qwerty13",
                    first_name="Andrew",
                    last_name="Johnson",
                    role=Role.EMPLOYEE,
                ),
            ),
            (
                "admin",
                "admin",
                UserCreateSchema(
                    login="ivan",
                    password="password12",
                    first_name="Ivan",
                    last_name="Vanov",
                    role=Role.ADMIN,
                ),
            ),
        ],
    )
    async def test_existing_login_creation(
        self,
        test_client: TestClient,
        db_manager: DatabaseManager,
        auth_service: AuthService,
        login: str,
        password: str,
        schema: UserCreateSchema,
    ):
        token = await get_auth_token(db_manager, auth_service, login, password)
        response = test_client.post(
            "/users",
            json=schema.model_dump(mode="json"),
            headers={"Authorization": f"Bearer {token}"},
        )

        assert response.status_code == status.HTTP_409_CONFLICT

    @pytest.mark.parametrize(
        "schema",
        [
            UserCreateSchema(
                login="adminer128",
                password="letmein",
                first_name="Andrew",
                last_name="Morozov",
                role=Role.ADMIN,
            ),
            UserCreateSchema(
                login="vanyok",
                password="parol",
                first_name="Vanya",
                last_name="Ivanov",
                role=Role.EMPLOYEE,
            ),
        ],
    )
    async def test_forbidden_by_anonymous_creation(
        self, test_client: TestClient, schema: UserCreateSchema
    ):
        response = test_client.post("/users", json=schema.model_dump(mode="json"))

        assert response.status_code == status.HTTP_403_FORBIDDEN

    @pytest.mark.parametrize(
        "login,password,schema",
        [
            (
                "super_misha",
                "gfhjkm",
                UserCreateSchema(
                    login="misha",
                    password="1234567890",
                    first_name="Misha",
                    last_name="Mishew",
                    role=Role.EMPLOYEE,
                ),
            ),
            (
                "linuxUser",
                "iusearchbtw",
                UserCreateSchema(
                    login="archiso",
                    password="archisthebest",
                    first_name="Arch",
                    last_name="Archer",
                    role=Role.ADMIN,
                ),
            ),
            (
                "petya",
                "qwerty12",
                UserCreateSchema(
                    login="petrov",
                    password="petr",
                    first_name="Petr",
                    last_name="Vasilyievich",
                    role=Role.MAIN_ADMIN,
                ),
            ),
        ],
    )
    async def test_forbidden_by_employee_creation(
        self,
        test_client: TestClient,
        db_manager: DatabaseManager,
        auth_service: AuthService,
        login: str,
        password: str,
        schema: UserCreateSchema,
    ):
        token = await get_auth_token(db_manager, auth_service, login, password)
        response = test_client.post(
            "/users",
            json=schema.model_dump(mode="json"),
            headers={"Authorization": f"Bearer {token}"},
        )

        assert response.status_code == status.HTTP_403_FORBIDDEN

    @pytest.mark.parametrize(
        "login,password,schema",
        [
            (
                "fisherman14",
                "fishingMaster",
                UserCreateSchema(
                    login="misha",
                    password="1234567890",
                    first_name="Misha",
                    last_name="Mishew",
                    role=Role.ADMIN,
                ),
            ),
            (
                "admin",
                "admin",
                UserCreateSchema(
                    login="archiso",
                    password="archisthebest",
                    first_name="Arch",
                    last_name="Archer",
                    role=Role.MAIN_ADMIN,
                ),
            ),
            (
                "ivan",
                "vanya2004",
                UserCreateSchema(
                    login="petrov",
                    password="petr",
                    first_name="Petr",
                    last_name="Vasilyievich",
                    role=Role.ADMIN,
                ),
            ),
        ],
    )
    async def test_forbidden_by_admin_creation(
        self,
        test_client: TestClient,
        db_manager: DatabaseManager,
        auth_service: AuthService,
        login: str,
        password: str,
        schema: UserCreateSchema,
    ):
        token = await get_auth_token(db_manager, auth_service, login, password)
        response = test_client.post(
            "/users",
            json=schema.model_dump(mode="json"),
            headers={"Authorization": f"Bearer {token}"},
        )

        assert response.status_code == status.HTTP_403_FORBIDDEN


class TestCurrentUserGet:
    @pytest.mark.parametrize(
        "login,password",
        [("dmitry337", "12345678"), ("anton11", "letmein"), ("foxy11", "foxest2005")],
    )
    async def test_valid_current_user(
        self,
        test_client: TestClient,
        db_manager: DatabaseManager,
        auth_service: AuthService,
        login: str,
        password: str,
    ):
        token = await get_auth_token(db_manager, auth_service, login, password)
        response = test_client.get(
            "/users/current", headers={"Authorization": f"Bearer {token}"}
        )

        assert response.is_success
        assert response.json()["login"] == login

    async def test_forbidden_by_anonymous_current_user(self, test_client: TestClient):
        response = test_client.get("/users/current")

        assert response.status_code == status.HTTP_403_FORBIDDEN


class TestUserGet:
    @pytest.mark.parametrize(
        "login,password,user_id",
        [
            ("admin", "admin", 1),
            ("admin", "admin", 5),
            ("admin", "admin", 9),
            ("ivan", "vanya2004", 1),
            ("ivan", "vanya2004", 5),
            ("ivan", "vanya2004", 9),
        ],
    )
    async def test_valid_user_get(
        self,
        test_client: TestClient,
        db_manager: DatabaseManager,
        auth_service: AuthService,
        login: str,
        password: str,
        user_id: int,
    ):
        token = await get_auth_token(db_manager, auth_service, login, password)
        response = test_client.get(
            f"/users/{user_id}",
            headers={"Authorization": f"Bearer {token}"},
        )

        assert response.is_success

        async with db_manager.create_session() as session:
            user_schema = UserSchema.model_validate(await session.get(User, user_id))

        result_schema = UserSchema.model_validate(response.json())
        assert result_schema.model_dump() == user_schema.model_dump()

    @pytest.mark.parametrize(
        "login,password,user_id",
        [
            ("admin", "admin", 50),
            ("admin", "admin", 112),
            ("admin", "admin", 560),
            ("ivan", "vanya2004", 48),
            ("ivan", "vanya2004", 99),
            ("ivan", "vanya2004", 678),
        ],
    )
    async def test_non_existing_user_get(
        self,
        test_client: TestClient,
        db_manager: DatabaseManager,
        auth_service: AuthService,
        login: str,
        password: str,
        user_id: int,
    ):
        token = await get_auth_token(db_manager, auth_service, login, password)
        response = test_client.get(
            f"/users/{user_id}",
            headers={"Authorization": f"Bearer {token}"},
        )

        assert response.status_code == status.HTTP_404_NOT_FOUND

    @pytest.mark.parametrize("user_id", [1, 3, 9, 57, 100])
    async def test_forbidden_by_anonymous_user_get(
        self, test_client: TestClient, user_id: int
    ):
        response = test_client.get(f"/users/{user_id}")

        assert response.status_code == status.HTTP_403_FORBIDDEN

    @pytest.mark.parametrize(
        "login,password,user_id",
        [
            ("tatyana1998", "password", 1),
            ("tatyana1998", "password", 291),
            ("anton11", "letmein", 4),
            ("anton11", "letmein", 197),
            ("super_misha", "gfhjkm", 9),
            ("super_misha", "gfhjkm", 57),
        ],
    )
    async def test_forbidden_by_employee_user_get(
        self,
        test_client: TestClient,
        db_manager: DatabaseManager,
        auth_service: AuthService,
        login: str,
        password: str,
        user_id: int,
    ):
        token = await get_auth_token(db_manager, auth_service, login, password)
        response = test_client.get(
            f"/users/{user_id}",
            headers={"Authorization": f"Bearer {token}"},
        )

        assert response.status_code == status.HTTP_403_FORBIDDEN


class TestUserGetAll:
    @pytest.mark.parametrize(
        "login,password,page,size",
        [
            ("ivan", "vanya2004", 0, 10),
            ("ivan", "vanya2004", 2, 5),
            ("ivan", "vanya2004", 3, 4),
            ("admin", "admin", 2, 4),
            ("admin", "admin", 1, 3),
            ("admin", "admin", 0, 5),
        ],
    )
    async def test_valid_get_all(
        self,
        test_client: TestClient,
        db_manager: DatabaseManager,
        auth_service: AuthService,
        login: str,
        password: str,
        page: int,
        size: int,
    ):
        token = await get_auth_token(db_manager, auth_service, login, password)
        response = test_client.get(
            "/users",
            params={"page": page, "size": size},
            headers={"Authorization": f"Bearer {token}"},
        )

        assert response.is_success

        async with db_manager.create_session() as session:
            total_users_count = (
                await session.execute(select(func.count()).select_from(User))
            ).scalar_one()
            users = (
                (await session.execute(select(User).offset(page * size).limit(size)))
                .scalars()
                .all()
            )

        result = PageSchema[UserSchema].model_validate(response.json())

        assert len(result.items) <= size
        assert result.total_items_count == total_users_count
        assert list(map(UserSchema.model_dump, result.items)) == list(
            map(lambda u: UserSchema.model_validate(u).model_dump(), users)
        )

    @pytest.mark.parametrize("page,size", [(0, 10), (2, 4), (5, 10), (4, 1)])
    async def test_forbidden_by_anonymous_get_all(
        self,
        test_client: TestClient,
        page: int,
        size: int,
    ):
        response = test_client.get(
            "/users",
            params={"page": page, "size": size},
        )

        assert response.status_code == status.HTTP_403_FORBIDDEN

    @pytest.mark.parametrize(
        "login,password,page,size",
        [
            ("petya", "qwerty12", 0, 10),
            ("dmitry337", "12345678", 2, 5),
            ("anton11", "letmein", 3, 4),
            ("linuxUser", "iusearchbtw", 2, 4),
        ],
    )
    async def test_forbidden_by_employee_get_all(
        self,
        test_client: TestClient,
        db_manager: DatabaseManager,
        auth_service: AuthService,
        login: str,
        password: str,
        page: int,
        size: int,
    ):
        token = await get_auth_token(db_manager, auth_service, login, password)
        response = test_client.get(
            "/users",
            params={"page": page, "size": size},
            headers={"Authorization": f"Bearer {token}"},
        )

        assert response.status_code == status.HTTP_403_FORBIDDEN


class TestCurrentUserUpdate:
    @pytest.mark.parametrize(
        "login,password,schema",
        [
            (
                "foxy11",
                "foxest2005",
                CurrentUserUpdateSchema(
                    login="foxy12", first_name="Nastya", last_name="Petrovna"
                ),
            ),
            (
                "fisherman14",
                "fishingMaster",
                CurrentUserUpdateSchema(
                    login="fisherman14", first_name="Andrew", last_name="Andreevich"
                ),
            ),
            (
                "admin",
                "admin",
                CurrentUserUpdateSchema(
                    login="admin", first_name="Admin", last_name="Admin"
                ),
            ),
        ],
    )
    async def test_valid_current_update(
        self,
        test_client: TestClient,
        db_manager: DatabaseManager,
        auth_service: AuthService,
        login: str,
        password: str,
        schema: CurrentUserUpdateSchema,
    ):
        token = await get_auth_token(db_manager, auth_service, login, password)
        response = test_client.put(
            "/users/current",
            headers={"Authorization": f"Bearer {token}"},
            json=schema.model_dump(mode="json"),
        )

        assert response.is_success

        response = test_client.get(
            "/users/current", headers={"Authorization": f"Bearer {token}"}
        )

        assert response.is_success

        result_schema = UserSchema.model_validate(response.json())
        assert result_schema.login == schema.login
        assert result_schema.first_name == schema.first_name
        assert result_schema.last_name == schema.last_name

        """ Tear down """
        async with db_manager.create_session() as session:
            user = (
                await session.execute(select(User).where(User.login == schema.login))
            ).scalar_one()
            user.login = login
            await session.flush()
            await session.commit()

    @pytest.mark.parametrize(
        "login,password,schema",
        [
            (
                "dmitry337",
                "12345678",
                CurrentUserUpdateSchema(
                    login="super_misha", first_name="Nastya", last_name="Petrovna"
                ),
            ),
            (
                "ivan",
                "vanya2004",
                CurrentUserUpdateSchema(
                    login="tatyana1998", first_name="Andrew", last_name="Andreevich"
                ),
            ),
            (
                "admin",
                "admin",
                CurrentUserUpdateSchema(
                    login="petya", first_name="Admin", last_name="Admin"
                ),
            ),
        ],
    )
    async def test_existing_login_current_update(
        self,
        test_client: TestClient,
        db_manager: DatabaseManager,
        auth_service: AuthService,
        login: str,
        password: str,
        schema: CurrentUserUpdateSchema,
    ):
        token = await get_auth_token(db_manager, auth_service, login, password)
        response = test_client.put(
            "/users/current",
            headers={"Authorization": f"Bearer {token}"},
            json=schema.model_dump(mode="json"),
        )

        assert response.status_code == status.HTTP_409_CONFLICT

    @pytest.mark.parametrize(
        "schema",
        [
            CurrentUserUpdateSchema(
                login="fisherman14", first_name="Andrew", last_name="Andreevich"
            ),
            CurrentUserUpdateSchema(
                login="anton11", first_name="Antosha", last_name="Antonov"
            ),
        ],
    )
    async def test_forbidden_by_anonymous_current_update(
        self, test_client: TestClient, schema: CurrentUserUpdateSchema
    ):
        response = test_client.put(
            "/users/current", json=schema.model_dump(mode="json")
        )

        assert response.status_code == status.HTTP_403_FORBIDDEN


class TestUserUpdate:
    @pytest.mark.parametrize(
        "login,password,user_id,schema",
        [
            (
                "ivan",
                "vanya2004",
                4,
                UserUpdateSchema(
                    login="dmitry337",
                    first_name="Dmitry",
                    last_name="Poganov",
                    role=Role.EMPLOYEE,
                ),
            ),
            (
                "ivan",
                "vanya2004",
                7,
                UserUpdateSchema(
                    login="super_misha_12",
                    first_name="Mishanya",
                    last_name="Michailov",
                    role=Role.EMPLOYEE,
                ),
            ),
            (
                "admin",
                "admin",
                8,
                UserUpdateSchema(
                    login="foxy12",
                    first_name="Foxy",
                    last_name="Foxest",
                    role=Role.ADMIN,
                ),
            ),
        ],
    )
    async def test_valid_update(
        self,
        test_client: TestClient,
        db_manager: DatabaseManager,
        auth_service: AuthService,
        login: str,
        password: str,
        user_id: int,
        schema: UserUpdateSchema,
    ):
        async with db_manager.create_session() as session:
            user = await session.get(User, user_id)
            user_data = {
                "login": user.login,
                "first_name": user.first_name,
                "last_name": user.last_name,
                "role": user.role,
            }

        token = await get_auth_token(db_manager, auth_service, login, password)
        response = test_client.put(
            f"/users/{user_id}",
            json=schema.model_dump(mode="json"),
            headers={"Authorization": f"Bearer {token}"},
        )

        assert response.is_success

        user_schema = UserSchema.model_validate(response.json())
        assert user_schema.login == schema.login
        assert user_schema.first_name == schema.first_name
        assert user_schema.last_name == schema.last_name
        assert user_schema.role == schema.role

        async with db_manager.create_session() as session:
            user = await session.get(User, user_id)
            user.login = user_data["login"]
            user.first_name = user_data["first_name"]
            user.last_name = user_data["last_name"]
            user.role = user_data["role"]
            await session.flush()
            await session.commit()

    @pytest.mark.parametrize(
        "login,password,user_id,schema",
        [
            (
                "ivan",
                "vanya2004",
                578,
                UserUpdateSchema(
                    login="dmitry337",
                    first_name="Dmitry",
                    last_name="Poganov",
                    role=Role.EMPLOYEE,
                ),
            ),
            (
                "ivan",
                "vanya2004",
                1337,
                UserUpdateSchema(
                    login="super_misha_12",
                    first_name="Mishanya",
                    last_name="Michailov",
                    role=Role.EMPLOYEE,
                ),
            ),
            (
                "admin",
                "admin",
                209,
                UserUpdateSchema(
                    login="foxy12",
                    first_name="Foxy",
                    last_name="Foxest",
                    role=Role.ADMIN,
                ),
            ),
        ],
    )
    async def test_non_existing_update(
        self,
        test_client: TestClient,
        db_manager: DatabaseManager,
        auth_service: AuthService,
        login: str,
        password: str,
        user_id: int,
        schema: UserUpdateSchema,
    ):
        token = await get_auth_token(db_manager, auth_service, login, password)
        response = test_client.put(
            f"/users/{user_id}",
            json=schema.model_dump(mode="json"),
            headers={"Authorization": f"Bearer {token}"},
        )

        assert response.status_code == status.HTTP_404_NOT_FOUND

    @pytest.mark.parametrize(
        "login,password,user_id,schema",
        [
            (
                "ivan",
                "vanya2004",
                2,
                UserUpdateSchema(
                    login="ivan",
                    first_name="Dmitry",
                    last_name="Poganov",
                    role=Role.EMPLOYEE,
                ),
            ),
            (
                "fisherman14",
                "fishingMaster",
                5,
                UserUpdateSchema(
                    login="anton11",
                    first_name="Mishanya",
                    last_name="Michailov",
                    role=Role.EMPLOYEE,
                ),
            ),
            (
                "admin",
                "admin",
                9,
                UserUpdateSchema(
                    login="linuxUser",
                    first_name="Foxy",
                    last_name="Foxest",
                    role=Role.ADMIN,
                ),
            ),
        ],
    )
    async def test_existing_login_update(
        self,
        test_client: TestClient,
        db_manager: DatabaseManager,
        auth_service: AuthService,
        login: str,
        password: str,
        user_id: int,
        schema: UserUpdateSchema,
    ):
        token = await get_auth_token(db_manager, auth_service, login, password)
        response = test_client.put(
            f"/users/{user_id}",
            json=schema.model_dump(mode="json"),
            headers={"Authorization": f"Bearer {token}"},
        )

        assert response.status_code == status.HTTP_409_CONFLICT

    @pytest.mark.parametrize(
        "user_id,schema",
        [
            (
                4,
                UserUpdateSchema(
                    login="dmitry337",
                    first_name="Dmitry",
                    last_name="Poganov",
                    role=Role.EMPLOYEE,
                ),
            ),
            (
                7,
                UserUpdateSchema(
                    login="super_misha_12",
                    first_name="Mishanya",
                    last_name="Michailov",
                    role=Role.EMPLOYEE,
                ),
            ),
            (
                8,
                UserUpdateSchema(
                    login="foxy12",
                    first_name="Foxy",
                    last_name="Foxest",
                    role=Role.ADMIN,
                ),
            ),
        ],
    )
    async def test_forbidden_by_anonymous_update(
        self,
        test_client: TestClient,
        user_id: int,
        schema: UserUpdateSchema,
    ):
        response = test_client.put(
            f"/users/{user_id}", json=schema.model_dump(mode="json")
        )

        assert response.status_code == status.HTTP_403_FORBIDDEN

    @pytest.mark.parametrize(
        "login,password,user_id,schema",
        [
            (
                "dmitry337",
                "12345678",
                4,
                UserUpdateSchema(
                    login="dmitry337",
                    first_name="Dmitry",
                    last_name="Poganov",
                    role=Role.EMPLOYEE,
                ),
            ),
            (
                "anton11",
                "letmein",
                7,
                UserUpdateSchema(
                    login="super_misha_12",
                    first_name="Mishanya",
                    last_name="Michailov",
                    role=Role.EMPLOYEE,
                ),
            ),
            (
                "linuxUser",
                "iusearchbtw",
                8,
                UserUpdateSchema(
                    login="foxy12",
                    first_name="Foxy",
                    last_name="Foxest",
                    role=Role.ADMIN,
                ),
            ),
        ],
    )
    async def test_forbidden_by_employee_update(
        self,
        test_client: TestClient,
        db_manager: DatabaseManager,
        auth_service: AuthService,
        login: str,
        password: str,
        user_id: int,
        schema: UserUpdateSchema,
    ):
        token = await get_auth_token(db_manager, auth_service, login, password)
        response = test_client.put(
            f"/users/{user_id}",
            json=schema.model_dump(mode="json"),
            headers={"Authorization": f"Bearer {token}"},
        )

        assert response.status_code == status.HTTP_403_FORBIDDEN

    @pytest.mark.parametrize(
        "login,password,user_id,schema",
        [
            (
                "ivan",
                "vanya2004",
                9,
                UserUpdateSchema(
                    login="fisherman14",
                    first_name="Fisherman",
                    last_name="Fisher",
                    role=Role.EMPLOYEE,
                ),
            ),
            (
                "fisherman14",
                "fishingMaster",
                3,
                UserUpdateSchema(
                    login="ivan10",
                    first_name="Vanya",
                    last_name="Vanyochek",
                    role=Role.ADMIN,
                ),
            ),
            (
                "ivan",
                "vanya2004",
                1,
                UserUpdateSchema(
                    login="admin",
                    first_name="Adminovich",
                    last_name="Adminov",
                    role=Role.MAIN_ADMIN,
                ),
            ),
            (
                "ivan",
                "vanya2004",
                5,
                UserUpdateSchema(
                    login="tatyana1998",
                    first_name="Tatyana",
                    last_name="Kireeva",
                    role=Role.ADMIN,
                ),
            ),
            (
                "fisherman14",
                "fishingMaster",
                6,
                UserUpdateSchema(
                    login="anton11",
                    first_name="Anton",
                    last_name="Antonovich",
                    role=Role.MAIN_ADMIN,
                ),
            ),
        ],
    )
    async def test_forbidden_by_admin_update(
        self,
        test_client: TestClient,
        db_manager: DatabaseManager,
        auth_service: AuthService,
        login: str,
        password: str,
        user_id: int,
        schema: UserUpdateSchema,
    ):
        token = await get_auth_token(db_manager, auth_service, login, password)
        response = test_client.put(
            f"/users/{user_id}",
            json=schema.model_dump(mode="json"),
            headers={"Authorization": f"Bearer {token}"},
        )

        assert response.status_code == status.HTTP_403_FORBIDDEN


class TestCurrentUserUpdatePassword:
    @pytest.mark.parametrize(
        "login,password,new_password",
        [
            ("anton11", "letmein", "qwerty31"),
            ("super_misha", "gfhjkm", "password"),
            ("admin", "admin", "admin"),
        ],
    )
    async def test_valid_current_update_password(
        self,
        test_client: TestClient,
        db_manager: DatabaseManager,
        auth_service: AuthService,
        login: str,
        password: str,
        new_password: str,
    ):
        token = await get_auth_token(db_manager, auth_service, login, password)
        response = test_client.put(
            "/users/current/password",
            json={"password": new_password},
            headers={"Authorization": f"Bearer {token}"},
        )

        assert response.is_success

        token = await get_auth_token(db_manager, auth_service, login, new_password)
        response = test_client.put(
            "/users/current/password",
            json={"password": password},
            headers={"Authorization": f"Bearer {token}"},
        )

        assert response.is_success

    @pytest.mark.parametrize("new_password", ["qwerty12", "12345678"])
    async def test_forbidden_by_anonymous_current_update_password(
        self,
        test_client: TestClient,
        new_password: str,
    ):
        response = test_client.put(
            "/users/current/password", json={"password": new_password}
        )

        assert response.status_code == status.HTTP_403_FORBIDDEN


class TestUserUpdatePassword:
    @pytest.mark.parametrize(
        "login,password,user_id,new_password",
        [
            ("ivan", "vanya2004", 5, "qwerty12"),
            ("admin", "admin", 3, "1234567890"),
        ],
    )
    async def test_valid_update_password(
        self,
        test_client: TestClient,
        db_manager: DatabaseManager,
        auth_service: AuthService,
        password_service: PasswordService,
        login: str,
        password: str,
        user_id: int,
        new_password: str,
    ):
        async with db_manager.create_session() as session:
            user = await session.get(User, user_id)
            old_hash = user.password_hash

        token = await get_auth_token(db_manager, auth_service, login, password)
        response = test_client.put(
            f"/users/{user_id}/password",
            json={"password": new_password},
            headers={"Authorization": f"Bearer {token}"},
        )

        assert response.is_success

        async with db_manager.create_session() as session:
            user = await session.get(User, user_id)
            assert password_service.compare_passwords(new_password, user.password_hash)

            user.password_hash = old_hash
            await session.flush()
            await session.commit()

    @pytest.mark.parametrize(
        "login,password,user_id,new_password",
        [
            ("ivan", "vanya2004", 58, "qwerty12"),
            ("admin", "admin", 124, "1234567890"),
        ],
    )
    async def test_non_existing_update_password(
        self,
        test_client: TestClient,
        db_manager: DatabaseManager,
        auth_service: AuthService,
        login: str,
        password: str,
        user_id: int,
        new_password: str,
    ):
        token = await get_auth_token(db_manager, auth_service, login, password)
        response = test_client.put(
            f"/users/{user_id}/password",
            json={"password": new_password},
            headers={"Authorization": f"Bearer {token}"},
        )

        assert response.status_code == status.HTTP_404_NOT_FOUND

    @pytest.mark.parametrize(
        "user_id,new_password",
        [(5, "qwerty12"), (3, "1234567890"), (1, "password")],
    )
    async def test_forbidden_by_anonymous_update_password(
        self,
        test_client: TestClient,
        user_id: int,
        new_password: str,
    ):
        response = test_client.put(
            f"/users/{user_id}/password", json={"password": new_password}
        )

        assert response.status_code == status.HTTP_403_FORBIDDEN

    @pytest.mark.parametrize(
        "login,password,user_id,new_password",
        [
            ("dmitry337", "12345678", 5, "qwerty12"),
            ("anton11", "letmein", 3, "1234567890"),
            ("super_misha", "gfhjkm", 1, "password"),
        ],
    )
    async def test_forbidden_by_employee_update_password(
        self,
        test_client: TestClient,
        db_manager: DatabaseManager,
        auth_service: AuthService,
        login: str,
        password: str,
        user_id: int,
        new_password: str,
    ):
        token = await get_auth_token(db_manager, auth_service, login, password)
        response = test_client.put(
            f"/users/{user_id}/password",
            json={"password": new_password},
            headers={"Authorization": f"Bearer {token}"},
        )

        assert response.status_code == status.HTTP_403_FORBIDDEN

    @pytest.mark.parametrize(
        "login,password,user_id,new_password",
        [
            ("fisherman14", "fishingMaster", 3, "qwerty12"),
            ("ivan", "vanya2004", 1, "password"),
            ("admin", "admin", 1, "123456"),
        ],
    )
    async def test_forbidden_by_admin_update_password(
        self,
        test_client: TestClient,
        db_manager: DatabaseManager,
        auth_service: AuthService,
        login: str,
        password: str,
        user_id: int,
        new_password: str,
    ):
        token = await get_auth_token(db_manager, auth_service, login, password)
        response = test_client.put(
            f"/users/{user_id}/password",
            json={"password": new_password},
            headers={"Authorization": f"Bearer {token}"},
        )

        assert response.status_code == status.HTTP_403_FORBIDDEN


class TestUserDelete:
    @pytest.mark.parametrize(
        "login,password,user_id",
        [
            ("ivan", "vanya2004", 6),
            ("ivan", "vanya2004", 8),
            ("admin", "admin", 9),
            ("admin", "admin", 7),
            ("admin", "admin", 3),
        ],
    )
    async def test_valid_delete(
        self,
        test_client: TestClient,
        db_manager: DatabaseManager,
        auth_service: AuthService,
        login: str,
        password: str,
        user_id: int,
    ):
        async with db_manager.create_session() as session:
            user = await session.get(User, user_id)
            employee: Employee = await user.awaitable_attrs.employee

            user_data = {
                "id": user.id,
                "login": user.login,
                "first_name": user.first_name,
                "last_name": user.last_name,
                "role": user.role,
                "password_hash": user.password_hash,
            }
            employee_data = (
                None
                if employee is None
                else {
                    "user_id": employee.user_id,
                    "salary_amount": employee.salary_amount,
                    "next_promotion_date": employee.next_promotion_date,
                    "update_date_time": employee.update_date_time,
                }
            )

        token = await get_auth_token(db_manager, auth_service, login, password)
        response = test_client.delete(
            f"/users/{user_id}", headers={"Authorization": f"Bearer {token}"}
        )
        assert response.is_success

        response = test_client.get(
            f"/users/{user_id}", headers={"Authorization": f"Bearer {token}"}
        )
        assert response.status_code == status.HTTP_404_NOT_FOUND

        async with db_manager.create_session() as session:
            session.add(
                User(
                    **user_data,
                    employee=(
                        None if employee_data is None else Employee(**employee_data)
                    ),
                )
            )
            await session.commit()

    @pytest.mark.parametrize(
        "login,password,user_id",
        [
            ("fisherman14", "fishingMaster", 50),
            ("fisherman14", "fishingMaster", 190),
            ("admin", "admin", 511),
            ("admin", "admin", 48),
        ],
    )
    async def test_non_existing_delete(
        self,
        test_client: TestClient,
        db_manager: DatabaseManager,
        auth_service: AuthService,
        login: str,
        password: str,
        user_id: int,
    ):
        token = await get_auth_token(db_manager, auth_service, login, password)
        response = test_client.delete(
            f"/users/{user_id}", headers={"Authorization": f"Bearer {token}"}
        )

        assert response.status_code == status.HTTP_404_NOT_FOUND

    @pytest.mark.parametrize("user_id", [1, 5, 10])
    async def test_forbidden_by_anonymous_delete(
        self, test_client: TestClient, user_id: int
    ):
        response = test_client.delete(f"/users/{user_id}")

        assert response.status_code == status.HTTP_403_FORBIDDEN

    @pytest.mark.parametrize(
        "login,password,user_id",
        [
            ("anton11", "letmein", 7),
            ("anton11", "letmein", 98),
            ("linuxUser", "iusearchbtw", 4),
            ("linuxUser", "iusearchbtw", 124),
            ("petya", "qwerty12", 507),
            ("petya", "qwerty12", 10),
        ],
    )
    async def test_forbidden_by_employee_delete(
        self,
        test_client: TestClient,
        db_manager: DatabaseManager,
        auth_service: AuthService,
        login: str,
        password: str,
        user_id: int,
    ):
        token = await get_auth_token(db_manager, auth_service, login, password)
        response = test_client.delete(
            f"/users/{user_id}", headers={"Authorization": f"Bearer {token}"}
        )

        assert response.status_code == status.HTTP_403_FORBIDDEN

    @pytest.mark.parametrize(
        "login,password,user_id",
        [
            ("admin", "admin", 1),
            ("ivan", "vanya2004", 9),
            ("fisherman14", "fishingMaster", 1),
        ],
    )
    async def test_forbidden_by_admin_delete(
        self,
        test_client: TestClient,
        db_manager: DatabaseManager,
        auth_service: AuthService,
        login: str,
        password: str,
        user_id: int,
    ):
        token = await get_auth_token(db_manager, auth_service, login, password)
        response = test_client.delete(
            f"/users/{user_id}", headers={"Authorization": f"Bearer {token}"}
        )

        assert response.status_code == status.HTTP_403_FORBIDDEN
