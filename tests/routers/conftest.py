import pytest
from fastapi import FastAPI
from fastapi.testclient import TestClient
from sqlalchemy import delete

from shift_cft_2024.auth.services import AuthService, PasswordService
from shift_cft_2024.core.db_manager import DatabaseManager
from shift_cft_2024.core.settings import AppSettings
from shift_cft_2024.core.setup import create_app
from shift_cft_2024.employee.models import Employee
from shift_cft_2024.users.models import User


@pytest.fixture(scope="session")
async def app():
    return create_app(
        AppSettings(
            db_url="sqlite+aiosqlite://",
            cors_allowed_origins=["*"],
            auth_token_secret_key="super_secret_key",
            initial_user=None,
        )
    )


@pytest.fixture(scope="session")
async def test_client(app: FastAPI, test_users: list[dict]):
    with TestClient(app) as client:
        db_manager: DatabaseManager = app.state.database_manager
        async with db_manager.create_session() as session:
            for user in test_users:
                user_data = user.copy()
                user_data["employee"] = (
                    Employee(**user_data["employee"])
                    if "employee" in user_data
                    else None
                )
                user_data.pop("password")
                session.add(User(**user_data))
            await session.commit()

        yield client

        async with db_manager.create_session() as session:
            await session.execute(delete(User))
            await session.commit()


@pytest.fixture(scope="session")
def db_manager(app: FastAPI) -> DatabaseManager:
    return app.state.database_manager


@pytest.fixture(scope="session")
def auth_service(app: FastAPI) -> AuthService:
    return app.state.auth_service


@pytest.fixture(scope="session")
def password_service(app: FastAPI) -> PasswordService:
    return app.state.password_service
