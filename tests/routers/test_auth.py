import pytest
from fastapi import status
from fastapi.testclient import TestClient

from shift_cft_2024.auth.services import AuthService
from shift_cft_2024.core.db_manager import DatabaseManager
from tests.routers.utils import get_auth_token


class TestLogin:
    @pytest.mark.parametrize(
        "login,password",
        [
            ("ivan", "vanya2004"),
            ("anton11", "letmein"),
            ("fisherman14", "fishingMaster"),
        ],
    )
    async def test_login(self, test_client: TestClient, login: str, password: str):
        response = test_client.post(
            "/auth/login", json={"login": login, "password": password}
        )

        assert response.is_success

        json: dict = response.json()
        assert "token" in json

        response = test_client.get(
            "/users/current", headers={"Authorization": f"Bearer {json['token']}"}
        )

        assert response.is_success

        json: dict = response.json()
        assert "login" in json and json["login"] == login

    @pytest.mark.parametrize(
        "login,password", [("super_misha", "qwerty12"), ("superuser", "12345678")]
    )
    async def test_login_invalid_credentials(
        self, test_client: TestClient, login: str, password: str
    ):
        response = test_client.post(
            "/auth/login", json={"login": login, "password": password}
        )

        assert response.status_code == status.HTTP_401_UNAUTHORIZED

    @pytest.mark.parametrize(
        "login,password",
        [("foxy11", "foxest2005"), ("anton11", "letmein"), ("admin", "admin")],
    )
    async def test_login_authorized(
        self,
        test_client: TestClient,
        auth_service: AuthService,
        db_manager: DatabaseManager,
        login: str,
        password: str,
    ):
        token = await get_auth_token(db_manager, auth_service, login, password)

        response = test_client.post(
            "/auth/login",
            json={"login": login, "password": password},
            headers={"Authorization": f"Bearer {token}"},
        )

        assert response.status_code == status.HTTP_403_FORBIDDEN
