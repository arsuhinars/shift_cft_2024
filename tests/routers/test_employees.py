from datetime import date

import pytest
from fastapi import status
from fastapi.testclient import TestClient
from sqlalchemy import func, select
from sqlalchemy.orm import joinedload

from shift_cft_2024.auth.services import AuthService
from shift_cft_2024.core.db_manager import DatabaseManager
from shift_cft_2024.core.schemas import PageSchema
from shift_cft_2024.employee.models import Employee
from shift_cft_2024.employee.schemas import (
    EmployeeFullSchema,
    EmployeeSchema,
    EmployeeUpdateSchema,
)
from shift_cft_2024.users.models import User
from tests.routers.utils import get_auth_token


class TestCurrentEmployeeGet:
    @pytest.mark.parametrize(
        "login,password",
        [("petya", "qwerty12"), ("anton11", "letmein"), ("super_misha", "gfhjkm")],
    )
    async def test_valid_current_employee_get(
        self,
        test_client: TestClient,
        db_manager: DatabaseManager,
        auth_service: AuthService,
        login: str,
        password: str,
    ):
        token = await get_auth_token(db_manager, auth_service, login, password)
        response = test_client.get(
            "/employees/current", headers={"Authorization": f"Bearer {token}"}
        )

        assert response.is_success

        async with db_manager.create_session() as session:
            employee_schema = EmployeeSchema.model_validate(
                await (await session.execute(select(User).where(User.login == login)))
                .scalar_one()
                .awaitable_attrs.employee
            )

        result_schema = EmployeeSchema.model_validate(response.json())
        assert result_schema.model_dump() == employee_schema.model_dump()

    @pytest.mark.parametrize(
        "login,password", [("foxy11", "foxest2005"), ("dmitry337", "12345678")]
    )
    async def test_non_existing_current_employee_get(
        self,
        test_client: TestClient,
        db_manager: DatabaseManager,
        auth_service: AuthService,
        login: str,
        password: str,
    ):
        token = await get_auth_token(db_manager, auth_service, login, password)
        response = test_client.get(
            "/employees/current", headers={"Authorization": f"Bearer {token}"}
        )

        assert response.status_code == status.HTTP_404_NOT_FOUND

    async def test_anonymous_current_employee_get(self, test_client: TestClient):
        response = test_client.get("/employees/current")

        assert response.status_code == status.HTTP_403_FORBIDDEN

    @pytest.mark.parametrize(
        "login,password",
        [("admin", "admin"), ("ivan", "vanya2004"), ("fisherman14", "fishingMaster")],
    )
    async def test_admin_current_employee_get(
        self,
        test_client: TestClient,
        db_manager: DatabaseManager,
        auth_service: AuthService,
        login: str,
        password: str,
    ):
        token = await get_auth_token(db_manager, auth_service, login, password)
        response = test_client.get(
            "/employees/current", headers={"Authorization": f"Bearer {token}"}
        )

        assert response.status_code == status.HTTP_403_FORBIDDEN


class TestEmployeeGet:
    @pytest.mark.parametrize(
        "login,password,user_id",
        [
            ("fisherman14", "fishingMaster", 7),
            ("admin", "admin", 10),
            ("ivan", "vanya2004", 5),
        ],
    )
    async def test_valid_employee_get(
        self,
        test_client: TestClient,
        db_manager: DatabaseManager,
        auth_service: AuthService,
        login: str,
        password: str,
        user_id: int,
    ):
        token = await get_auth_token(db_manager, auth_service, login, password)
        response = test_client.get(
            f"/employees/{user_id}", headers={"Authorization": f"Bearer {token}"}
        )

        assert response.is_success

        async with db_manager.create_session() as session:
            employee_schema = EmployeeSchema.model_validate(
                (
                    await session.execute(
                        select(Employee).where(Employee.user_id == user_id)
                    )
                ).scalar_one()
            )

        result_schema = EmployeeSchema.model_validate(response.json())

        assert result_schema == employee_schema

    @pytest.mark.parametrize(
        "login,password,user_id",
        [
            ("fisherman14", "fishingMaster", 4),
            ("admin", "admin", 125),
            ("admin", "admin", 8),
            ("ivan", "vanya2004", 9),
            ("ivan", "vanya2004", 57),
        ],
    )
    async def test_non_existing_employee_get(
        self,
        test_client: TestClient,
        db_manager: DatabaseManager,
        auth_service: AuthService,
        login: str,
        password: str,
        user_id: int,
    ):
        token = await get_auth_token(db_manager, auth_service, login, password)
        response = test_client.get(
            f"/employees/{user_id}", headers={"Authorization": f"Bearer {token}"}
        )

        assert response.status_code == status.HTTP_404_NOT_FOUND

    @pytest.mark.parametrize("user_id", [8, 10, 9, 59, 105])
    async def test_anonymous_employee_get(
        self,
        test_client: TestClient,
        user_id: int,
    ):
        response = test_client.get(f"/employees/{user_id}")

        assert response.status_code == status.HTTP_403_FORBIDDEN

    @pytest.mark.parametrize(
        "login,password,user_id",
        [
            ("foxy11", "foxest2005", 4),
            ("anton11", "letmein", 5),
            ("linuxUser", "iusearchbtw", 8),
            ("dmitry337", "12345678", 9),
            ("petya", "qwerty12", 7),
            ("petya", "qwerty12", 58),
        ],
    )
    async def test_by_employee_employee_get(
        self,
        test_client: TestClient,
        db_manager: DatabaseManager,
        auth_service: AuthService,
        login: str,
        password: str,
        user_id: int,
    ):
        token = await get_auth_token(db_manager, auth_service, login, password)
        response = test_client.get(
            f"/employees/{user_id}", headers={"Authorization": f"Bearer {token}"}
        )

        assert response.status_code == status.HTTP_403_FORBIDDEN


class TestEmployeeGetAll:
    @pytest.mark.parametrize(
        "login,password,page,size",
        [
            ("fisherman14", "fishingMaster", 0, 10),
            ("fisherman14", "fishingMaster", 2, 4),
            ("admin", "admin", 3, 4),
            ("ivan", "vanya2004", 1, 8),
        ],
    )
    async def test_valid_employee_get_all(
        self,
        test_client: TestClient,
        db_manager: DatabaseManager,
        auth_service: AuthService,
        login: str,
        password: str,
        page: int,
        size: int,
    ):
        token = await get_auth_token(db_manager, auth_service, login, password)
        response = test_client.get(
            "/employees",
            params={"page": page, "size": size},
            headers={"Authorization": f"Bearer {token}"},
        )

        assert response.is_success

        async with db_manager.create_session() as session:
            total_employees_count = (
                await session.execute(select(func.count()).select_from(Employee))
            ).scalar_one()
            employees = list(
                map(
                    EmployeeFullSchema.model_validate,
                    (
                        await session.execute(
                            select(Employee)
                            .offset(page * size)
                            .limit(size)
                            .options(joinedload(Employee.user))
                        )
                    )
                    .scalars()
                    .all(),
                )
            )

        result = PageSchema[EmployeeFullSchema].model_validate(response.json())

        assert result.total_items_count == total_employees_count
        assert result.items == employees

    @pytest.mark.parametrize("page,size", [(0, 10), (2, 10), (3, 4), (1, 5)])
    async def test_anonymous_employee_get_all(
        self,
        test_client: TestClient,
        page: int,
        size: int,
    ):
        response = test_client.get("/employees", params={"page": page, "size": size})

        assert response.status_code == status.HTTP_403_FORBIDDEN

    @pytest.mark.parametrize(
        "login,password,page,size",
        [
            ("dmitry337", "12345678", 0, 10),
            ("tatyana1998", "password", 2, 4),
            ("super_misha", "gfhjkm", 3, 4),
            ("anton11", "letmein", 1, 8),
        ],
    )
    async def test_by_employee_employee_get_all(
        self,
        test_client: TestClient,
        db_manager: DatabaseManager,
        auth_service: AuthService,
        login: str,
        password: str,
        page: int,
        size: int,
    ):
        token = await get_auth_token(db_manager, auth_service, login, password)
        response = test_client.get(
            "/employees",
            params={"page": page, "size": size},
            headers={"Authorization": f"Bearer {token}"},
        )

        assert response.status_code == status.HTTP_403_FORBIDDEN


class TestEmployeeUpdate:
    @pytest.mark.parametrize(
        "login,password,user_id,schema",
        [
            (
                "admin",
                "admin",
                4,
                EmployeeUpdateSchema(
                    salary_amount=40_000, next_promotion_date=date(2024, 5, 9)
                ),
            ),
            (
                "ivan",
                "vanya2004",
                6,
                EmployeeUpdateSchema(
                    salary_amount=65_000, next_promotion_date=date(2024, 9, 17)
                ),
            ),
            (
                "fisherman14",
                "fishingMaster",
                8,
                EmployeeUpdateSchema(
                    salary_amount=10_000, next_promotion_date=date(2024, 7, 25)
                ),
            ),
        ],
    )
    async def test_valid_employee_update(
        self,
        test_client: TestClient,
        db_manager: DatabaseManager,
        auth_service: AuthService,
        login: str,
        password: str,
        user_id: int,
        schema: EmployeeUpdateSchema,
    ):
        token = await get_auth_token(db_manager, auth_service, login, password)
        response = test_client.put(
            f"/employees/{user_id}",
            json=schema.model_dump(mode="json"),
            headers={"Authorization": f"Bearer {token}"},
        )

        assert response.is_success

        async with db_manager.create_session() as session:
            employee = (
                await session.execute(
                    select(Employee).where(Employee.user_id == user_id)
                )
            ).scalar_one()

        assert employee.salary_amount == schema.salary_amount
        assert employee.next_promotion_date == schema.next_promotion_date

    @pytest.mark.parametrize(
        "login,password,user_id,schema",
        [
            (
                "admin",
                "admin",
                15,
                EmployeeUpdateSchema(
                    salary_amount=40_000, next_promotion_date=date(2024, 5, 9)
                ),
            ),
            (
                "ivan",
                "vanya2004",
                48,
                EmployeeUpdateSchema(
                    salary_amount=65_000, next_promotion_date=date(2024, 9, 17)
                ),
            ),
            (
                "fisherman14",
                "fishingMaster",
                52,
                EmployeeUpdateSchema(
                    salary_amount=10_000, next_promotion_date=date(2024, 7, 25)
                ),
            ),
        ],
    )
    async def test_non_existing_employee_update(
        self,
        test_client: TestClient,
        db_manager: DatabaseManager,
        auth_service: AuthService,
        login: str,
        password: str,
        user_id: int,
        schema: EmployeeUpdateSchema,
    ):
        token = await get_auth_token(db_manager, auth_service, login, password)
        response = test_client.put(
            f"/employees/{user_id}",
            json=schema.model_dump(mode="json"),
            headers={"Authorization": f"Bearer {token}"},
        )

        assert response.status_code == status.HTTP_404_NOT_FOUND

    @pytest.mark.parametrize(
        "login,password,user_id,schema",
        [
            (
                "admin",
                "admin",
                9,
                EmployeeUpdateSchema(
                    salary_amount=40_000, next_promotion_date=date(2024, 5, 9)
                ),
            ),
            (
                "ivan",
                "vanya2004",
                1,
                EmployeeUpdateSchema(
                    salary_amount=65_000, next_promotion_date=date(2024, 9, 17)
                ),
            ),
            (
                "fisherman14",
                "fishingMaster",
                3,
                EmployeeUpdateSchema(
                    salary_amount=10_000, next_promotion_date=date(2024, 7, 25)
                ),
            ),
        ],
    )
    async def test_non_employee_employee_update(
        self,
        test_client: TestClient,
        db_manager: DatabaseManager,
        auth_service: AuthService,
        login: str,
        password: str,
        user_id: int,
        schema: EmployeeUpdateSchema,
    ):
        token = await get_auth_token(db_manager, auth_service, login, password)
        response = test_client.put(
            f"/employees/{user_id}",
            json=schema.model_dump(mode="json"),
            headers={"Authorization": f"Bearer {token}"},
        )

        assert response.status_code == status.HTTP_403_FORBIDDEN

    @pytest.mark.parametrize(
        "user_id,schema",
        [
            (
                4,
                EmployeeUpdateSchema(
                    salary_amount=40_000, next_promotion_date=date(2024, 5, 9)
                ),
            ),
            (
                6,
                EmployeeUpdateSchema(
                    salary_amount=65_000, next_promotion_date=date(2024, 9, 17)
                ),
            ),
            (
                8,
                EmployeeUpdateSchema(
                    salary_amount=10_000, next_promotion_date=date(2024, 7, 25)
                ),
            ),
        ],
    )
    async def test_anonymous_employee_update(
        self,
        test_client: TestClient,
        user_id: int,
        schema: EmployeeUpdateSchema,
    ):
        response = test_client.put(
            f"/employees/{user_id}", json=schema.model_dump(mode="json")
        )

        assert response.status_code == status.HTTP_403_FORBIDDEN

    @pytest.mark.parametrize(
        "login,password,user_id,schema",
        [
            (
                "foxy11",
                "foxest2005",
                4,
                EmployeeUpdateSchema(
                    salary_amount=40_000, next_promotion_date=date(2024, 5, 9)
                ),
            ),
            (
                "anton11",
                "letmein",
                6,
                EmployeeUpdateSchema(
                    salary_amount=65_000, next_promotion_date=date(2024, 9, 17)
                ),
            ),
            (
                "dmitry337",
                "12345678",
                8,
                EmployeeUpdateSchema(
                    salary_amount=10_000, next_promotion_date=date(2024, 7, 25)
                ),
            ),
        ],
    )
    async def test_by_employee_employee_update(
        self,
        test_client: TestClient,
        db_manager: DatabaseManager,
        auth_service: AuthService,
        login: str,
        password: str,
        user_id: int,
        schema: EmployeeUpdateSchema,
    ):
        token = await get_auth_token(db_manager, auth_service, login, password)
        response = test_client.put(
            f"/employees/{user_id}",
            json=schema.model_dump(mode="json"),
            headers={"Authorization": f"Bearer {token}"},
        )

        assert response.status_code == status.HTTP_403_FORBIDDEN
