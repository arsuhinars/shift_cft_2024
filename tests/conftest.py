from datetime import date, datetime

import pytest

from shift_cft_2024.auth.services import PasswordService
from shift_cft_2024.users.models import Role

TEST_USERS = [
    {
        "id": 1,
        "login": "admin",
        "password": "admin",
        "first_name": "First Name",
        "last_name": "Last Name",
        "role": Role.MAIN_ADMIN,
    },
    {
        "id": 2,
        "login": "petya",
        "password": "qwerty12",
        "first_name": "Peter",
        "last_name": "Petrov",
        "role": Role.EMPLOYEE,
        "employee": {
            "salary_amount": 10_000,
            "next_promotion_date": date(2024, 10, 5),
            "update_date_time": datetime(2024, 9, 15, 20, 30),
        },
    },
    {
        "id": 3,
        "login": "ivan",
        "password": "vanya2004",
        "first_name": "Ivan",
        "last_name": "Ivanov",
        "role": Role.ADMIN,
    },
    {
        "id": 4,
        "login": "dmitry337",
        "password": "12345678",
        "first_name": "Dimitry",
        "last_name": "Dimitriev",
        "role": Role.EMPLOYEE,
    },
    {
        "id": 5,
        "login": "tatyana1998",
        "password": "password",
        "first_name": "Tatyana",
        "last_name": "Kireeva",
        "role": Role.EMPLOYEE,
        "employee": {
            "salary_amount": 30_000,
            "next_promotion_date": date(2024, 10, 12),
            "update_date_time": datetime(2024, 9, 15, 20, 45),
        },
    },
    {
        "id": 6,
        "login": "anton11",
        "password": "letmein",
        "first_name": "Anton",
        "last_name": "Antonovich",
        "role": Role.EMPLOYEE,
        "employee": {
            "salary_amount": 25_000,
            "next_promotion_date": date(2024, 10, 5),
            "update_date_time": datetime(2024, 9, 15, 19, 20),
        },
    },
    {
        "id": 7,
        "login": "super_misha",
        "password": "gfhjkm",
        "first_name": "Michael",
        "last_name": "Michailovich",
        "role": Role.EMPLOYEE,
        "employee": {
            "salary_amount": 45_000,
            "next_promotion_date": date(2024, 11, 1),
            "update_date_time": datetime(2024, 9, 16, 16, 15),
        },
    },
    {
        "id": 8,
        "login": "foxy11",
        "password": "foxest2005",
        "first_name": "Anastasiya",
        "last_name": "Petrova",
        "role": Role.EMPLOYEE,
    },
    {
        "id": 9,
        "login": "fisherman14",
        "password": "fishingMaster",
        "first_name": "Andrey",
        "last_name": "Andreevich",
        "role": Role.ADMIN,
    },
    {
        "id": 10,
        "login": "linuxUser",
        "password": "iusearchbtw",
        "first_name": "Anatoliy",
        "last_name": "Anatoliyevich",
        "role": Role.EMPLOYEE,
        "employee": {
            "salary_amount": 80_000,
            "next_promotion_date": date(2024, 11, 14),
            "update_date_time": datetime(2024, 9, 20, 17, 00),
        },
    },
]


@pytest.fixture(scope="session")
def token_secret_key():
    return "super_secret_key"


@pytest.fixture(scope="session")
def password_service():
    return PasswordService()


@pytest.fixture(scope="session")
def test_users(password_service: PasswordService):
    test_users = list(map(dict.copy, TEST_USERS))
    for user in test_users:
        user["password_hash"] = password_service.get_password_hash(user["password"])

    return test_users
