from datetime import date

import pytest
from sqlalchemy.ext.asyncio import AsyncSession

from shift_cft_2024.core.exceptions import EntityNotFoundException, ForbiddenException
from shift_cft_2024.employee.repositories import EmployeeRepository
from shift_cft_2024.employee.schemas import EmployeeSchema, EmployeeUpdateSchema
from shift_cft_2024.employee.services import EmployeeService
from shift_cft_2024.users.repositories import UserRepository


@pytest.fixture
def employee_service(
    user_repository: UserRepository, employee_repository: EmployeeRepository
):
    return EmployeeService(user_repository, employee_repository)


@pytest.mark.parametrize("user_id", [2, 5, 6])
async def test_get_employee(
    async_session: AsyncSession,
    employee_service: EmployeeService,
    employee_repository: EmployeeRepository,
    user_id: int,
):
    employee = await employee_repository.get_by_user_id(async_session, user_id)
    result = await employee_service.get_by_user_id(async_session, user_id)

    assert (
        EmployeeSchema.model_validate(result).model_dump()
        == EmployeeSchema.model_validate(employee).model_dump()
    )


@pytest.mark.parametrize("user_id", [4, 8, 9])
async def test_get_invalid_employee(
    async_session: AsyncSession,
    employee_service: EmployeeService,
    user_id: int,
):
    with pytest.raises(EntityNotFoundException):
        await employee_service.get_by_user_id(async_session, user_id)


@pytest.mark.parametrize(
    "page,size", [(0, 4), (1, 4), (0, 5), (1, 5), (0, 10), (4, 2), (8, 1)]
)
async def test_get_all(
    async_session: AsyncSession,
    employee_service: EmployeeService,
    employee_repository: EmployeeRepository,
    page: int,
    size: int,
):
    employees_count = await employee_repository.count_all(async_session)
    employees = await employee_repository.get_all(async_session, page, size)
    result = await employee_service.get_all(async_session, page, size)

    assert len(result.items) <= size
    assert result.total_items_count == employees_count
    assert result.items == list(employees)


@pytest.mark.parametrize(
    "user_id,salary_amount,next_promotion_date",
    [
        (8, 15_000, date(2024, 12, 5)),
        (6, 50_000, date(2024, 11, 8)),
        (4, 100_000, date(2024, 6, 25)),
    ],
)
async def test_update_employee(
    async_session: AsyncSession,
    employee_service: EmployeeService,
    employee_repository: EmployeeRepository,
    user_id: int,
    salary_amount: int,
    next_promotion_date: date,
):
    schema = EmployeeUpdateSchema(
        salary_amount=salary_amount, next_promotion_date=next_promotion_date
    )
    await employee_service.update_by_user_id(async_session, user_id, schema)

    employee = await employee_repository.get_by_user_id(async_session, user_id)

    assert employee is not None
    assert employee.salary_amount == salary_amount
    assert employee.next_promotion_date == next_promotion_date


@pytest.mark.parametrize(
    "user_id,salary_amount,next_promotion_date",
    [
        (9, 40_000, date(2024, 5, 18)),
        (3, 35_000, date(2024, 10, 17)),
        (1, 50_000, date(2024, 11, 29)),
    ],
)
async def test_update_invalid_employee(
    async_session: AsyncSession,
    employee_service: EmployeeService,
    user_id: int,
    salary_amount: int,
    next_promotion_date: date,
):
    schema = EmployeeUpdateSchema(
        salary_amount=salary_amount, next_promotion_date=next_promotion_date
    )

    with pytest.raises(ForbiddenException):
        await employee_service.update_by_user_id(async_session, user_id, schema)


@pytest.mark.parametrize(
    "user_id,salary_amount,next_promotion_date",
    [
        (12, 40_000, date(2024, 5, 18)),
        (57, 35_000, date(2024, 10, 17)),
        (112, 50_000, date(2024, 11, 29)),
    ],
)
async def test_update_non_existing_employee(
    async_session: AsyncSession,
    employee_service: EmployeeService,
    user_id: int,
    salary_amount: int,
    next_promotion_date: date,
):
    schema = EmployeeUpdateSchema(
        salary_amount=salary_amount, next_promotion_date=next_promotion_date
    )

    with pytest.raises(EntityNotFoundException):
        await employee_service.update_by_user_id(async_session, user_id, schema)
