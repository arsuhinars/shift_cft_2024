import pytest
from sqlalchemy.ext.asyncio import AsyncSession

from shift_cft_2024.auth.services import PasswordService
from shift_cft_2024.core.exceptions import (
    EntityAlreadyExistsException,
    EntityNotFoundException,
)
from shift_cft_2024.users.models import Role
from shift_cft_2024.users.repositories import UserRepository
from shift_cft_2024.users.schemas import UserCreateSchema, UserSchema, UserUpdateSchema
from shift_cft_2024.users.services import UserService


@pytest.fixture
def user_service(password_service: PasswordService, user_repository: UserRepository):
    return UserService(password_service, user_repository)


@pytest.mark.parametrize(
    "login,password,first_name,last_name,role",
    [
        ("vanya1992", "vanyok", "Ivan", "Ivanovich", Role.EMPLOYEE),
        ("nastyenka", "nastena1998", "Anastasiya", "Stasyevna", Role.EMPLOYEE),
        ("fox_lady", "mega_foxy_228", "Vladislava", "Slavovna", Role.ADMIN),
    ],
)
async def test_valid_creation(
    async_session: AsyncSession,
    user_repository: UserRepository,
    user_service: UserService,
    login: str,
    password: str,
    first_name: str,
    last_name: str,
    role: Role,
):
    schema = UserCreateSchema(
        login=login,
        password=password,
        first_name=first_name,
        last_name=last_name,
        role=role,
    )

    user = await user_service.create(async_session, schema)
    created_user = await user_repository.get_by_id(async_session, user.id)

    assert (
        UserSchema.model_validate(created_user).model_dump()
        == UserSchema.model_validate(user).model_dump()
    )


@pytest.mark.parametrize(
    "login,password,first_name,last_name,role",
    [
        ("anton11", "123456789", "Ivan", "Ivanov", Role.EMPLOYEE),
        ("petya", "qwefewrt43", "Petr", "Peter", Role.EMPLOYEE),
        ("admin", "superuser", "Admin", "Adminovich", Role.ADMIN),
    ],
)
async def test_existing_login_creation(
    async_session: AsyncSession,
    user_service: UserService,
    login: str,
    password: str,
    first_name: str,
    last_name: str,
    role: Role,
):
    schema = UserCreateSchema(
        login=login,
        password=password,
        first_name=first_name,
        last_name=last_name,
        role=role,
    )

    with pytest.raises(EntityAlreadyExistsException):
        await user_service.create(async_session, schema)


@pytest.mark.parametrize("user_id", [4, 3, 8])
async def test_valid_get_by_id(
    async_session: AsyncSession,
    user_service: UserService,
    user_repository: UserRepository,
    user_id: int,
):
    user = await user_repository.get_by_id(async_session, user_id)
    result = await user_service.get_by_id(async_session, user.id)

    assert (
        UserSchema.model_validate(result).model_dump()
        == UserSchema.model_validate(user).model_dump()
    )


@pytest.mark.parametrize("user_id", [11, 54, 98, 112])
async def test_non_existing_get_by_id(
    async_session: AsyncSession,
    user_service: UserService,
    user_id: int,
):
    with pytest.raises(EntityNotFoundException):
        await user_service.get_by_id(async_session, user_id)


@pytest.mark.parametrize("page,size", [(1, 1), (2, 2), (0, 5), (0, 10), (0, 5), (1, 5)])
async def test_get_all(
    async_session: AsyncSession,
    user_service: UserService,
    user_repository: UserRepository,
    page: int,
    size: int,
):
    users_count = await user_repository.count_all(async_session)
    users = await user_repository.get_all(async_session, page, size)
    result = await user_service.get_all(async_session, page, size)

    assert len(result.items) <= size
    assert result.total_items_count == users_count
    assert list(users) == result.items


@pytest.mark.parametrize(
    "user_id,login,first_name,last_name,role",
    [
        (6, "anton11", "Antov", "Prohorov", Role.ADMIN),
        (8, "nastya", "Anastasiya", "Kireeva", Role.EMPLOYEE),
        (2, "petr", "Petr", "Petrovich", Role.EMPLOYEE),
    ],
)
async def test_valid_update(
    async_session: AsyncSession,
    user_service: UserService,
    user_repository: UserRepository,
    user_id: int,
    login: str,
    first_name: str,
    last_name: str,
    role: Role,
):
    schema = UserUpdateSchema(
        login=login,
        first_name=first_name,
        last_name=last_name,
        role=role,
    )
    await user_service.update_by_id(async_session, user_id, schema)
    result = await user_repository.get_by_id(async_session, user_id)

    assert result is not None
    assert result.login == login
    assert result.first_name == first_name
    assert result.last_name == last_name
    assert result.role == role


@pytest.mark.parametrize(
    "user_id,login,first_name,last_name,role",
    [
        (11, "petya", "Peter", "Petrovich", Role.EMPLOYEE),
        (97, "ivan", "Vanya", "Ivanovich", Role.ADMIN),
        (112, "tatyana2004", "Tanya", "Kireeva", Role.EMPLOYEE),
    ],
)
async def test_non_existing_update(
    async_session: AsyncSession,
    user_service: UserService,
    user_id: int,
    login: str,
    first_name: str,
    last_name: str,
    role: Role,
):
    schema = UserUpdateSchema(
        login=login,
        first_name=first_name,
        last_name=last_name,
        role=role,
    )

    with pytest.raises(EntityNotFoundException):
        await user_service.update_by_id(async_session, user_id, schema)


@pytest.mark.parametrize(
    "user_id,login,first_name,last_name,role",
    [
        (1, "anton11", "First Name", "Admin", Role.MAIN_ADMIN),
        (7, "dmitry337", "Michael", "Michailovich", Role.EMPLOYEE),
        (10, "foxy11", "Anatloliy", "Anatoliyevich", Role.EMPLOYEE),
    ],
)
async def test_existing_login_update(
    async_session: AsyncSession,
    user_service: UserService,
    user_id: int,
    login: str,
    first_name: str,
    last_name: str,
    role: Role,
):
    schema = UserUpdateSchema(
        login=login,
        first_name=first_name,
        last_name=last_name,
        role=role,
    )

    with pytest.raises(EntityAlreadyExistsException):
        await user_service.update_by_id(async_session, user_id, schema)


@pytest.mark.parametrize(
    "user_id,password",
    [
        (7, "balaba"),
        (10, "hacker337"),
        (4, "super_dmitry"),
    ],
)
async def test_update_password(
    async_session: AsyncSession,
    user_service: UserService,
    password_service: PasswordService,
    user_repository: UserRepository,
    user_id: int,
    password: str,
):
    await user_service.update_password_by_id(async_session, user_id, password)

    user = await user_repository.get_by_id(async_session, user_id)
    assert password_service.compare_passwords(password, user.password_hash)


@pytest.mark.parametrize(
    "user_id,password",
    [
        (11, "power_off"),
        (512, "mega_hacker"),
        (237, "qwerty12"),
    ],
)
async def test_non_existing_update_password(
    async_session: AsyncSession, user_service: UserService, user_id: int, password: str
):
    with pytest.raises(EntityNotFoundException):
        await user_service.update_password_by_id(async_session, user_id, password)


@pytest.mark.parametrize("user_id", [1, 2, 5, 10])
async def test_valid_delete(
    async_session: AsyncSession,
    user_service: UserService,
    user_repository: UserRepository,
    user_id: int,
):
    await user_service.delete_by_id(async_session, user_id)

    user = await user_repository.get_by_id(async_session, user_id)
    assert user is None


@pytest.mark.parametrize("user_id", [112, 1024, 997])
async def test_non_existing_delete(
    async_session: AsyncSession, user_service: UserService, user_id: int
):
    with pytest.raises(EntityNotFoundException):
        await user_service.delete_by_id(async_session, user_id)
