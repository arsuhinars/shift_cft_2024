# flake8: noqa F401
from datetime import date, datetime
from typing import Any
from unittest.mock import MagicMock

import pytest
from sqlalchemy.ext.asyncio import AsyncSession

from shift_cft_2024.auth.services import PasswordService
from shift_cft_2024.employee.models import Employee
from shift_cft_2024.employee.repositories import EmployeeRepository
from shift_cft_2024.users.models import Role, User
from shift_cft_2024.users.repositories import UserRepository


@pytest.fixture(scope="package")
def async_session() -> AsyncSession:
    return MagicMock()


class FakeEmployeeRepository(EmployeeRepository):
    def __init__(self):
        self.__db: dict[int, Employee] = {}
        self.__id_counter = 0

    async def get_by_user_id(self, session: AsyncSession, user_id: int):
        return self.__db.get(user_id)

    async def get_all(self, session: AsyncSession, page: int, size: int):
        offset = page * size
        return list(self.__db.values())[offset : offset + size]

    async def count_all(self, session: AsyncSession):
        return len(self.__db)

    async def save(self, session: AsyncSession, employee: Employee):
        if employee.id is None:
            employee.id = self.__id_counter + 1

        self.__db[employee.user_id] = employee
        self.__id_counter = max(employee.id, self.__id_counter)
        return employee

    async def delete(self, session: AsyncSession, employee: Employee):
        self.__db.pop(employee.user_id, None)


class FakeUserRepository(UserRepository):
    def __init__(self, employee_repository: FakeEmployeeRepository):
        self.__db: dict[int, User] = {}
        self.__id_counter = 0
        self.__employee_repository = employee_repository

    @property
    def id_counter(self):
        return self.__id_counter

    async def get_by_id(self, session: AsyncSession, id: int):
        return self.__db.get(id)

    async def get_by_login(self, session: AsyncSession, login: str):
        result = filter(lambda u: u.login == login, self.__db.values())
        try:
            return result.__next__()
        except StopIteration:
            return None

    async def exists_by_login(self, session: AsyncSession, login: str):
        return await self.get_by_login(session, login) is not None

    async def get_all(self, session: AsyncSession, page: int, size: int):
        offset = page * size
        return list(self.__db.values())[offset : offset + size]

    async def count_all(self, session: AsyncSession):
        return len(self.__db)

    async def save(self, session: AsyncSession, user: User):
        if user.id is None:
            user.id = self.__id_counter + 1

        if user.employee is not None:
            user.employee.user_id = user.id
            await self.__employee_repository.save(session, user.employee)
        else:
            db_user = self.__db.get(user.id)
            if db_user is not None and db_user.employee is not None:
                await self.__employee_repository.delete(session, user.employee)

        self.__db[user.id] = user
        self.__id_counter = max(user.id, self.__id_counter)
        return user

    async def delete(self, session: AsyncSession, user: User):
        del self.__db[user.id]


@pytest.fixture
async def employee_repository():
    return FakeEmployeeRepository()


@pytest.fixture
async def user_repository(
    async_session: AsyncSession,
    test_users: list[dict],
    employee_repository: EmployeeRepository,
):
    repository = FakeUserRepository(employee_repository)
    for user in test_users:
        user_data = user.copy()
        user_data["employee"] = (
            Employee(**user_data["employee"]) if "employee" in user_data else None
        )
        user_data.pop("password")
        await repository.save(async_session, User(**user_data))

    return repository
