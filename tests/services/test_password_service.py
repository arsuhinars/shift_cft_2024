import pytest

from shift_cft_2024.auth.services import PasswordService


@pytest.mark.parametrize(
    "password,compare_password",
    [
        ("aba", "baba"),
        ("cadaba", "cadaba"),
        ("qwerty12", "Qwerty12"),
        ("password", "password12"),
        ("test", "password123"),
        ("pass", "pass"),
        ("test", "test"),
    ],
)
def test_password_service(
    password: str, compare_password: str, password_service: PasswordService
):
    hashed_password = password_service.get_password_hash(password)
    assert password_service.compare_passwords(compare_password, hashed_password) == (
        password == compare_password
    )
