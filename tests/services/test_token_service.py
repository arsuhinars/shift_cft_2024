from datetime import UTC, datetime, timedelta

import jwt
import pytest

from shift_cft_2024.auth.schemas import AuthTokenPayload
from shift_cft_2024.auth.services import TokenService


@pytest.fixture(scope="module")
def token_service(token_secret_key: str):
    return TokenService(token_secret_key, 3600)


@pytest.mark.parametrize("user_id", [1, 5, 7, 9])
def test_token_creation(
    token_secret_key: str, token_service: TokenService, user_id: int
):
    initial_payload = AuthTokenPayload(user_id=user_id)
    token = token_service.create_auth_token(initial_payload)

    payload = jwt.decode(token, token_secret_key, algorithms=["HS256", "RS256"])
    payload = AuthTokenPayload.model_validate(payload)

    assert payload.model_dump() == initial_payload.model_dump()


@pytest.mark.parametrize("user_id", [1, 5, 7, 9])
def test_verify_valid_token(
    token_secret_key: str, token_service: TokenService, user_id: int
):
    initial_payload = AuthTokenPayload(user_id=user_id)

    claims = initial_payload.model_dump()
    claims["exp"] = datetime.now() + timedelta(seconds=10.0)
    token = jwt.encode(claims, token_secret_key)

    payload = token_service.verify_auth_token(token)
    assert payload is not None
    assert payload.model_dump() == initial_payload.model_dump()


@pytest.mark.parametrize(
    "invalid_token",
    [
        "va7CiPzuGmwPHysH0i9PnD7PASW31Yst",
        "khlk5FvvrICiGx5EKTJ8ouD028zUYv5b",
        "0erWZpLtnYekXrvDQcs1PpP5WnoMIMNP",
    ],
)
def test_verify_invalid_token(token_service: TokenService, invalid_token: str):
    token = token_service.verify_auth_token(invalid_token)
    assert token is None


@pytest.mark.parametrize(
    "invalid_token_key,user_id",
    [
        ("invalid_secret_key", 1),
        ("invalid_secret_key", 5),
        ("not_super_secret_key", 7),
        ("not_super_secret_key", 10),
    ],
)
def test_verify_invalid_signed_token(
    token_service: TokenService, invalid_token_key: str, user_id: int
):
    initial_payload = AuthTokenPayload(user_id=user_id)

    claims = initial_payload.model_dump()
    claims["exp"] = datetime.now() + timedelta(seconds=10.0)
    token = jwt.encode(claims, invalid_token_key)

    payload = token_service.verify_auth_token(token)
    assert payload is None


@pytest.mark.parametrize("user_id", [1, 5, 4, 7])
def test_verify_expired_token(
    token_secret_key: str, token_service: TokenService, user_id: int
):
    initial_payload = AuthTokenPayload(user_id=user_id)

    claims = initial_payload.model_dump()
    claims["exp"] = datetime.now(UTC) - timedelta(seconds=10.0)
    token = jwt.encode(claims, token_secret_key)

    payload = token_service.verify_auth_token(token)
    assert payload is None
