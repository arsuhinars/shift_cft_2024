from datetime import UTC, datetime, timedelta

import jwt
import pytest
from sqlalchemy.ext.asyncio import AsyncSession

from shift_cft_2024.auth.schemas import AuthTokenPayload, LoginCredentials
from shift_cft_2024.auth.services import AuthService, PasswordService, TokenService
from shift_cft_2024.core.exceptions import UnauthorizedException
from shift_cft_2024.users.repositories import UserRepository
from shift_cft_2024.users.schemas import UserSchema


@pytest.fixture(scope="module")
def token_service(token_secret_key: str):
    return TokenService(token_secret_key, 3600)


@pytest.fixture
def auth_service(
    password_service: PasswordService,
    token_service: TokenService,
    user_repository: UserRepository,
):
    return AuthService(password_service, token_service, user_repository)


@pytest.mark.parametrize(
    "login,password",
    [
        ("dmitry337", "12345678"),
        ("fisherman14", "fishingMaster"),
        ("foxy11", "foxest2005"),
        ("admin", "admin"),
    ],
)
async def test_login_valid_credentials(
    async_session: AsyncSession,
    auth_service: AuthService,
    token_service: TokenService,
    user_repository: UserRepository,
    login: str,
    password: str,
):
    user = await user_repository.get_by_login(async_session, login)
    token_schema = await auth_service.login_user(
        async_session, LoginCredentials(login=login, password=password)
    )
    payload = token_service.verify_auth_token(token_schema.token)

    assert payload is not None
    assert payload.user_id == user.id


@pytest.mark.parametrize(
    "login,password",
    [
        ("petya", "qwerty123"),
        ("super_misha", "12345678"),
        ("linuxUser", "linuxoid"),
        ("anton11", "password"),
    ],
)
async def test_login_invalid_password(
    async_session: AsyncSession,
    auth_service: AuthService,
    token_service: TokenService,
    login: str,
    password: str,
):
    with pytest.raises(UnauthorizedException):
        await auth_service.login_user(
            async_session, LoginCredentials(login=login, password=password)
        )


@pytest.mark.parametrize(
    "login,password",
    [
        ("mega_destroyer", "qwerty12"),
        ("vanya", "vanya2004"),
        ("linuxoid", "iusearchbtw"),
        ("super_michail", "gfhjkm"),
    ],
)
async def test_login_invalid_login(
    async_session: AsyncSession,
    auth_service: AuthService,
    login: str,
    password: str,
):
    with pytest.raises(UnauthorizedException):
        await auth_service.login_user(
            async_session, LoginCredentials(login=login, password=password)
        )


@pytest.mark.parametrize("user_id", [1, 5, 9])
async def test_authenticate_valid_token(
    async_session: AsyncSession,
    token_secret_key: str,
    auth_service: AuthService,
    user_repository: UserRepository,
    user_id: int,
):
    payload = AuthTokenPayload(user_id=user_id).model_dump()
    payload["exp"] = datetime.now(UTC) + timedelta(seconds=10.0)
    token = jwt.encode(payload, token_secret_key)

    user = await user_repository.get_by_id(async_session, user_id)
    auth_user = await auth_service.authenticate_user(async_session, token)
    assert auth_user.id == user_id
    assert (
        UserSchema.model_validate(auth_user).model_dump()
        == UserSchema.model_validate(user).model_dump()
    )


@pytest.mark.parametrize(
    "invalid_token",
    [
        "pEpgl8JpPLe8bxvavc9sWRvUaqIIYCL3",
        "vgvnxJjVJbLjh3LcrIaj2CVyc61o6djC",
        "2s0qaOv20a2EPMqlbiZfWreZxYRFZfU3",
    ],
)
async def test_authenticate_invalid_token(
    async_session: AsyncSession, auth_service: AuthService, invalid_token: str
):
    with pytest.raises(UnauthorizedException):
        await auth_service.authenticate_user(async_session, invalid_token)


@pytest.mark.parametrize("user_id", [0, 12, 150, 1957])
async def test_authenticate_deleted_user(
    async_session: AsyncSession,
    token_secret_key: str,
    auth_service: AuthService,
    user_id: int,
):
    payload = AuthTokenPayload(user_id=user_id).model_dump()
    payload["exp"] = datetime.now(UTC) + timedelta(seconds=10.0)
    token = jwt.encode(payload, token_secret_key)

    with pytest.raises(UnauthorizedException):
        await auth_service.authenticate_user(async_session, token)


@pytest.mark.parametrize("user_id", [1, 5, 3, 7])
async def test_authenticate_expired_token(
    async_session: AsyncSession,
    token_secret_key: str,
    auth_service: AuthService,
    user_id: int,
):
    payload = AuthTokenPayload(user_id=user_id).model_dump()
    payload["exp"] = datetime.now(UTC) - timedelta(seconds=10.0)
    token = jwt.encode(payload, token_secret_key)

    with pytest.raises(UnauthorizedException):
        await auth_service.authenticate_user(async_session, token)
